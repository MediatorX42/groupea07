package Persistance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import model.deck.Card;

public class Persistance {
	/**
	 * Methode permettant d'écrire un String dans un fichier
	 * @param txt = texte a ecrire
	 * @param nomF = nom du fichier
	 */
	public static void ecrireTxt(String txt, String nomF)
	{
		try(FileWriter f = new FileWriter(new File(nomF)))
		{
			
			f.write(txt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode permettant de lire un object dans un fichier json
	 * @param <T> = classe de l'objet
	 * @param nomF = nom du fichier
	 * @param classOfT = object Class correspondant a la classe de l'objet
	 * @return un Objet de la classe spécifier
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 * @throws FileNotFoundException
	 */
	public static <T> T lireJson(String nomF, Class<T> classOfT) throws JsonIOException, JsonSyntaxException, FileNotFoundException
	{
		Gson g = new Gson();
		
		JsonObject jsonObj = JsonParser.parseReader(new FileReader(nomF)).getAsJsonObject(); 
		T c = g.fromJson(jsonObj, classOfT);
		return c;
		
		
	}
	
	/**
	 * Methode permettant d'ecrire un object dans un fichier json
	 * @param <T> = classe de l'objet
	 * @param path = chemin du fichier
	 * @param object = objet a ecrire 
	 */
	public static <T> void ecrireJson(String path, T object) {
		try(Writer out = new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8)) {
			new GsonBuilder()
			.disableHtmlEscaping()
			.setPrettyPrinting()
			.create()
			.toJson(object, out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

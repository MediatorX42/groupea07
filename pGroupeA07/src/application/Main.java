package application;
	


import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import view.GameViewSP;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			// Creation du layout principal
			GameViewSP root = new GameViewSP(primaryStage);
			
			// Creation de la scene contenant le layout principal
			Scene scene = new Scene(root);
						
			// Retrait de la possibilite de redimentionner la fenetre, ajout de la scene au stage,
			// affichage du stage
			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.setOnCloseRequest(ev->{
				Platform.exit();
				System.exit(0);
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	 
	public static void main(String[] args) {
		launch(args);
	}
}
package connexion;

import java.io.FileNotFoundException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import Persistance.Persistance;
import exception.AlreadyPresentException;
import exception.EmailIncorectException;
import exception.UserNameOrEmailTakenException;
/**
 * Permet de gerer la connexion d'un utilisateur
 * @author natha
 *
 */
public class ConnexionControl {

	/**
	 * test si une combinaisons userName/password existe dans le fichier users.json
	 */
	public static boolean control(String userName, String password) {
		UserManagement userManagement = null;
		try {
			//initialisation avec les users déja enregistré
			userManagement = Persistance.lireJson("res/users.json", UserManagement.class);
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		//On verifie qu'un utilisateur avec cette combinaison de nom/pass existe
		User userConnect = new User(userName, String.valueOf(password.hashCode()));
		for (User u : userManagement.getUtilisateurs()) {
			if(u.equals(userConnect) && u.getPassword().equals(userConnect.getPassword())) {
				
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Créer un nouvel utilisateur, lui envoie un mail de confirmation et l'enregistre dans le fichier users.json
	 *
	 * @param userName = Nom du nouvel utilisateur
	 * @param password = mot de passe du nouvel utilisateur 
	 * @param userMail = Mail du nouvel utilisateur
	 * @return true si l'enregistrement a réussi
	 * @throws UserNameOrEmailTakenException
	 * @throws EmailIncorectException
	 */
	public static boolean registerNewUser(String userName, String password, String userMail) throws UserNameOrEmailTakenException, EmailIncorectException {
		UserManagement userManagement = null;
		//initialisation avec les users déja enregistré
		try {
			userManagement = Persistance.lireJson("res/users.json", UserManagement.class);
			
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//on ajoute un nouvel utilisateur a la liste d'user enregistré
		try {
			userManagement.ajouter(new User(userName, String.valueOf(password.hashCode()), userMail));
			if(MailUtils.send(userMail, userName))
				{
					Persistance.ecrireJson("res/users.json", userManagement);
					return true;
				}
				throw new EmailIncorectException();
		} catch (AlreadyPresentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		throw new UserNameOrEmailTakenException();
	}
}

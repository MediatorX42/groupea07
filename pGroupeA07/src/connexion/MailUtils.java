package connexion;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 * Permet d'envoyer un mail
 * @author natha
 *
 */
public class MailUtils {

	/**
	 * Envoie un mail 
	 * @param to adresse mail du destinataire
	 * @param pseudo pseudo du destinataire
	 * @return true si l'envoie a réussi
	 */
	public static boolean send(String to, String pseudo) {
		// Variable initialiser dans la méthode pour plus de sécurité
		String from = "galileotheofficialvideogame@gmail.com";
		String pwd = "NathanCedric42";
		String obj = "Galileo";
		String msg = "Welcome to Galileo and thanks for your registration !";
		// Configuration de base
		Properties p = new Properties();
		
		p.put("mail.smtp.host", "smtp.gmail.com");
		p.put("mail.smtp.starttls.enable", "true");
		p.put("mail.smtp.ssl.protocols", "TLSv1.2");
	    p.put("mail.smtp.socketFactory.port", "465");
	    p.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    p.put("mail.smtp.auth", "true");
	    p.put("mail.smtp.port", "465");
	    
	    // Ouverture d'une session 
	    Session s = Session.getDefaultInstance(p, new Authenticator() {
	      		protected PasswordAuthentication getPasswordAuthentication() {
	      			return new PasswordAuthentication(from, pwd);
	      		}
	    });
	    
	    // Message
	    MimeMessage m = new MimeMessage(s);
	    try {
			m.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			m.setSubject(obj);
			m.setText("Hey " + pseudo + " !\n\n" + msg);
			
			// Envoi
			Transport.send(m);
			System.out.println("Message envoyé avec succès");
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
}

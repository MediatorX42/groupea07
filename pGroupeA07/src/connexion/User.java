package connexion;
/**
 * Classe qui représente un utilisateur
 * @author natha
 *
 */
public class User {
	private String userName;
	private String password;
	private String mail;
	
	public User(String userName, String password, String mail) {
		super();
		this.userName = userName;
		this.password = password;
		this.mail = mail;
	}
	
	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
		this.mail = "DEFAULT";
	}
	
	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + ", mail=" + mail +"]";
	}

	@Override
	public boolean equals(Object obj) {
		User o = (User) obj;
		return this.userName.equals(o.userName) || this.mail.equals(o.mail);
	}
	
	public User clone() {
		return new User(userName, password, mail);
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getMail() {
		return mail;
	}
}
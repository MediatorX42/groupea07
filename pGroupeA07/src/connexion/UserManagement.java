package connexion;

import java.util.ArrayList;
import java.util.List;

import exception.AlreadyPresentException;
/**
 * Classe de gestion d'utilisateur
 * @author natha
 *
 */
public class UserManagement {
	List<User> utilisateurs;
	public UserManagement() {
		this.utilisateurs = new ArrayList<>(); 
	}
	
	public void ajouter(User u) throws AlreadyPresentException{
		if(!utilisateurs.contains(u))
			utilisateurs.add(u.clone());
		else 
			throw new AlreadyPresentException();
	}
	
	public List<User> getUtilisateurs() {
		List<User> tmp = new ArrayList<>();
		for (User user : utilisateurs) {
			tmp.add(user.clone());
		}
		return tmp;
	};

}
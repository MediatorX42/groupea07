package enumeration;

import java.io.Serializable;
/**
 * Enssembble des catégorie de question
 * @author natha
 *
 */
public enum Category implements Serializable{
	IDEAS("#9370DB", 0),
	SCIENCE("#ffA500", 1),
	PLANET("#2E8B57", 2),
	HISTORY("#eb1a8d", 3),
	LITERATURE("#1E29FD", 4),
	COMPUTER("#71413B", 5);
	
	private String color;
	private int indice;
	
	private Category(String color, int indice) {
		this.color = color;
		this.indice = indice;
	}
	
	public String getColor() {
		return color;
	}

	public int getIndice() {
		return indice;
	}
}

package exception;
/**
 * Signifie qu'une liste contient déja l'élement a ajouter
 * @author natha
 *
 */
public class AlreadyPresentException extends Exception {
	public AlreadyPresentException()
	{
		super("This element already exist in this list");
	}
}

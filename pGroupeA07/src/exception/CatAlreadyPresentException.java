package exception;
/**
 * Signifie qu'une carte contient déja une question de cette catégorie
 * @author natha
 *
 */
public class CatAlreadyPresentException extends Exception {
	public CatAlreadyPresentException() {
		super("This card already contain a question for this category");
	}
}

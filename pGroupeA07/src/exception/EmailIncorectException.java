package exception;
/**
 * Signifie que l'email est incorect
 * @author natha
 *
 */
public class EmailIncorectException extends Exception{
	public EmailIncorectException()
	{
		super("Invalid email");
	}
}

package exception;
/**
 * Signifie qu'on ajoute un deuxieme choix pouvant etre correct a une question
 * @author natha
 *
 */
public class OnlyOneTrueException extends Exception {
	public OnlyOneTrueException()
	{
		super("Only one choice can be true !");
	}
}

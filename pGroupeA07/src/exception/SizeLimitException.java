package exception;

public class SizeLimitException extends Exception {
	/**
	 * Signifie qu'on a déja assez d'élement dans une liste 
	 */
	public SizeLimitException()
	{
		super("The list has reach her limit size !");
	}
}

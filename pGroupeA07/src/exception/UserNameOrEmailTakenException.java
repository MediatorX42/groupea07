package exception;

public class UserNameOrEmailTakenException extends Exception{
	/**
	 * Signifie qu'un utilisateur avec cette combinaison de pseudo/pass existe deja
	 */
	public UserNameOrEmailTakenException()
	{
		super("Username or e-mail adress already used");
	}
}

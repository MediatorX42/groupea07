package media;

import java.nio.file.Paths;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicManager {

	private MediaPlayer musicPlayer;
	private double musicValue;
	private MediaPlayer music2Player;
	
	
	public MusicManager() {
		this.musicValue = 1;
	}

	/**
	 * joue la musique du jeu
	 */
	public void music() {
		String s = "res/musique/ingame.wav";
		Media h = new Media(Paths.get(s).toUri().toString());
		//création d'un mediaPlayer et modification de ses paramètres
		musicPlayer = new MediaPlayer(h);
		musicPlayer.play();
		musicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		musicPlayer.setVolume(musicValue);
	
	}
	/**
	 * joue la musique du menu
	 */
	public void music2() {
		String s = "res/musique/menu.wav";
		Media h = new Media(Paths.get(s).toUri().toString());
		//création d'un mediaPlayer et modification de ses paramètres
		music2Player = new MediaPlayer(h);
		music2Player.play();
		music2Player.setOnEndOfMedia(() -> {
			music();
		});
		music2Player.setVolume(musicValue);
		
	}
	
	/**
	 * Méthode qui change le son de la musique
	 * @param volume
	 */
	public void setVolume(double volume) {
		if(music2Player != null)
			music2Player.setVolume(volume);
		
		if(musicPlayer != null)
			musicPlayer.setVolume(volume);
		
		musicValue = volume;
	}

	public double getMusicValue() {
		return musicValue;
	}
	
}

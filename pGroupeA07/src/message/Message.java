package message;

import java.io.Serializable;

/**
 *  La classe Message cree des operations composes d'une operation et d'une missive (objet)
 * @author natha
 *
 */
public class Message implements Serializable{

	private Operation operation;
	private Object missive;
	
	public Message(Operation operation, Object missive) {
		super();
		this.operation = operation;
		this.missive = missive;
	}
	
	public Operation getOperation() {
		return operation;
	}

	public Object getMissive() {
		return missive;
	}
}
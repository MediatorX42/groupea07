package message;

import java.io.Serializable;

/**
 *  Liste de toutes les operations possibles pour un message
 * @author natha
 *
 */
public enum Operation implements Serializable{
	CONNECT,
	DISCONNECT,
	MESSAGE,
	READY, 
	STARTGAME,
	ROLL,
	ON_TRUE_ANSWER,
	ON_FALSE_ANSWER,
	QUESTION_CATEGORY,
	BONUS_CATEGORY,
	MALUS_CATEGORY,
	NEXT_TURN;
}
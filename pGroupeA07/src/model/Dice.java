package model;
/**
 * contient les chemins vers les images de chaque face du dé
 * @author natha
 *
 */
public enum Dice {
	D1("d1.png"),
	D2("d2.png"),
	D3("d3.png"),
	D4("d4.png"),
	D5("d5.png"),
	D6("d6.png");
	
	String s;
	Dice(String s) {
		this.s = s;
	}
	public String getS() {
		return s;
	}
}

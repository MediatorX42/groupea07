package model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Persistance.Persistance;
import enumeration.Category;
import model.deck.Card;
import model.deck.Deck;
import model.deck.Question;
/**
 * classe de recuperation et de reorganisation des decks
 * @author natha
 *
 */
public class GameManager {

	private List<Deck> decks;
	private List<String> decksName;
	private List< List<Question> > listOfCategories;
	private List<Question> alreadyUseQuestion;
	private List<Question> questIdeas;
	private List<Question> questScience;
	private List<Question> questPlanet;
	private List<Question> questHistory;
	private List<Question> questLiterature;
	private List<Question> questComputer;
	private static final int NB_CATEGORY = 6;
	private Random r;
	
	public GameManager()
	{
		r = new Random();
		// Recuperation de chaque decks dans le dossier deck
		File f = new File("res/deck");
		File listf[] = f.listFiles();
		decks = new ArrayList<>();
		decksName = new ArrayList<>();
		for (File file : listf) {
			try {
				Deck tmp = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
				if(tmp.verify() && !(tmp.toString().isBlank()))
				{
					
					decks.add(tmp);
					decksName.add(file.getName().substring(0, file.getName().length() - 5));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		

		
		// Initialisations
		listOfCategories = new ArrayList<>();
		alreadyUseQuestion = new ArrayList<>();
		this.questIdeas = new ArrayList<>();
		this.questScience = new ArrayList<>();
		this.questPlanet = new ArrayList<>();
		this.questHistory = new ArrayList<>();
		this.questLiterature = new ArrayList<>();
		this.questComputer = new ArrayList<>();
		
		// Ajout de chaque liste dans la super liste
		listOfCategories.add(questIdeas);
		listOfCategories.add(questScience);
		listOfCategories.add(questPlanet);
		listOfCategories.add(questHistory);
		listOfCategories.add(questLiterature);
		listOfCategories.add(questComputer);
		
		classificationsQuestions(decks);
	}
	
	/**
	 * Cette methode permet de selectionner une question aleatoire au sein d'une categorie
	 * @return Question Un objet de type question
	 */
	public Question randomQuestion(Category cat)
	{
		// On stocke la liste correspondant a la categorie choisie
		List<Question> choosenList = listOfCategories.get(cat.getIndice());
		
		// Si il ne reste plus qu'une question dans la liste, on la remplit avec les questions deja sorties
		if (choosenList.size() <= 1)
		{
			List<Question> alreadyUseQuestionListClone = new ArrayList<>();
			for (Question q : alreadyUseQuestion) {
				
				if (q.getCategory() == choosenList.get(0).getCategory())
				{
					alreadyUseQuestionListClone.add(q);
				}
			}
			
			// On remplit la liste de questions avec celle deja utilisees
			choosenList.addAll(alreadyUseQuestionListClone);
			
			// On enleve toutes les questions de cette categorie de la liste des questions deja utilisees
			alreadyUseQuestion.removeAll(alreadyUseQuestionListClone);
			
			// On renvoie la derniere question, donc on l'enleve de la liste et on la rajoute a la liste de questions deja utilisees
			Question questionToReturn = choosenList.get(0);
			alreadyUseQuestion.add(questionToReturn);
			choosenList.remove(questionToReturn);
			return choosenList.get(0);
		}
		else
		{
			// On choisit une question au hasard dans cette categorie 
			int randQuest = 1+r.nextInt(choosenList.size()-1);
			Question questionToReturn = choosenList.get(randQuest);
			
			// On retire la question choisie de la liste des questions et on l'ajoute a la liste des questions deja utilisees
			choosenList.remove(questionToReturn);
			alreadyUseQuestion.add(questionToReturn);
			return questionToReturn;
		}
	}
	
	/** 
	 * Cette methode permet de trier les questions de chaque deck par categorie
	 * @param decks Liste de decks recue en argument
	 */
	public void classificationsQuestions(List<Deck> decks)
	{
		this.decks = decks;
		// Recuperation de la liste des cartes
		for (Deck d : decks) {
			
			List<Card> cards = d.getCards();
			
			for (Card card : cards) {
				
				//Recuperation de la liste des questions
				List<Question> questions = card.getQuestions();
				for (Question q : questions) {
					
					// Filtrage des questions pour les ajouter dans la liste correspondante a leurs categories
					switch (q.getCategory()) {
						case IDEAS: questIdeas.add(q);break;
						case SCIENCE : questScience.add(q); break;
						case PLANET : questPlanet.add(q); break;
						case HISTORY : questHistory.add(q); break;
						case LITERATURE : questLiterature.add(q); break;
						case COMPUTER : questComputer.add(q); break;
					}
				}
			}
		}
	}
	
	public List<Deck> getDecks() {
		List<Deck> tmp = new ArrayList<>();
		for (Deck d : decks) {
			tmp.add(d);
		}
		return tmp;
	}
	
	public List<String> getDecksName() {
		List<String> tmp = new ArrayList<>();
		System.out.println(decksName);
		for (String d : decksName) {
			tmp.add(d);
		}
		return tmp;
	}
	
}

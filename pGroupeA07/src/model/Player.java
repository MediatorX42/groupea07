package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import enumeration.Category;
/**
 * represente un joueur au cours d'une partie
 * @author natha
 *
 */
public class Player {
	private String name;
	private int score;
	private HashMap<Category, Boolean> categoriesValidation;
	private int indCase;
	private boolean frozen = false;
	
	private static final int POINT_PER_QUESTION = 50;
	
	
	public Player(String name)
	{
		this.name = name;
		this.score = 0;
		
		//initialisation de la liste des categorie validï¿½
		categoriesValidation = new HashMap<>();
		categoriesValidation.put(Category.IDEAS, false);
		categoriesValidation.put(Category.SCIENCE, false);
		categoriesValidation.put(Category.PLANET, false);
		categoriesValidation.put(Category.HISTORY, false);
		categoriesValidation.put(Category.LITERATURE, false);
		categoriesValidation.put(Category.COMPUTER, false);
	}
	
	/**
	 * Valide une categorie
	 * @param cat = catégorie a valider
	 */
	public void validateCat(Category cat)
	{
		if(categoriesValidation.containsKey(cat)) categoriesValidation.put(cat, true);
	}
	
	/**
	 * Enleve une categorie validé par le joueur
	 * @param cat = la catégorie a enlever
	 */
	public void RemoveCat(Category cat)
	{
		if(categoriesValidation.containsKey(cat)) categoriesValidation.put(cat, false);
	}
	
	public String getName() {
		return name;
	}
	
	public Set<Map.Entry<Category, Boolean> > getCategoriesValidation()
	{
		return categoriesValidation.entrySet();
	}
	
	public int getScore() {
		return score;
	}
	
	/**
	 * Augmente le score du joueur d'un montant donné par la constante POINT_PER_QUESTION
	 */
	public void increaseScore()
	{
		score += POINT_PER_QUESTION;
	}
	
	public int getIndCase()
	{
		return indCase;
	}
	
	public void setIndCase(int ind)
	{
		this.indCase = ind;
	}
	public void setFrozen(boolean state)
	{
		frozen = state;
	}
	
	public boolean isFrozen()
	{
		return frozen;
	}
	
	/**
	 * Verifie si un joueur a valider une catégorie précise
	 * @param category = la catégorie a tester
	 * @return
	 */
	public boolean havevalidateCat(Category category) {
		// TODO Auto-generated method stub
		for (Map.Entry<Category, Boolean> entry : getCategoriesValidation()) {
			if(category.name().equals(entry.getKey().name()))
			{
				return entry.getValue();
			}
		}
		return false;
	}
	
	
}

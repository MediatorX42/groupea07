package model.deck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import exception.AlreadyPresentException;
import exception.CatAlreadyPresentException;
import exception.OnlyOneTrueException;
import exception.SizeLimitException;
/**
 * Represente une carte compose de question
 * @author natha
 *
 */
public class Card implements Serializable{
	
	private List<Question> questions;
	public static final int MAX_QUESTIONS = 6;

	public Card() {
		questions = new ArrayList<>();
	}
	
	/**
	 * Ajoute une nouvelle question a la carte
	 * @param q = nouvelle question
	 * @throws AlreadyPresentException
	 * @throws CatAlreadyPresentException
	 * @throws SizeLimitException
	 */
	public void addQuestion(Question q) throws AlreadyPresentException, CatAlreadyPresentException, SizeLimitException
	{
		if(questions.size() >= MAX_QUESTIONS) throw new SizeLimitException();
		if(questions.contains(q)) throw new AlreadyPresentException();
		for (Question question : questions) {
			if (q.getCategory() == question.getCategory()) throw new CatAlreadyPresentException();
		}
		questions.add(q);
	}
	
	/**
	 * Supprime une question de la carte
	 * @param q = nouvelle question
	 * @return
	 */
	public boolean deleteQuestion(Question q)
	{
		return questions.remove(q);
	}
	
	/**
	 * met a jour l'auteur d'une question
	 * @param oldQ ancienne question
	 * @param newQ question qui remplacera la nouvelle
	 * @return
	 */
	public boolean updateAuthor(Question oldQ, Question newQ)
	{
		if(questions.contains(oldQ))
		{
			Question q = questions.get(questions.indexOf(oldQ));
			q.setAuthor(newQ.getAuthor());
			
			return true;
		
		}
		return false;
	}
	
	/**
	 * Met a jour la catégorie et l'interogation d'une question
	 * @param oldQ ancienne question
	 * @param newQ nouvelle question
	 * @return
	 */
	public boolean updateCatAndIntero(Question oldQ, Question newQ)
	{
		if(questions.contains(oldQ) && !questions.contains(newQ))
		{
			Question q = questions.get(questions.indexOf(oldQ));
			q.setCategory(newQ.getCategory());
			q.setInterrogation(newQ.getInterrogation());
			return true;
			
		}
		return false;
	}
	
	/**
	 * Met a jour les choix d'une question
	 * @param q = question a modifier
	 * @param newChoices = nouveau choix
	 * @return
	 */
	public boolean updateChoices(Question q, HashMap<String, Boolean> newChoices)
	{
		if(questions.contains(q))
		{
			Question quest = questions.get(questions.indexOf(q));
			quest.clearChoices();
			Set<Map.Entry<String, Boolean> > parcours = newChoices.entrySet();
			for (Map.Entry<String, Boolean> entry : parcours) {
				try {
					quest.addChoices(entry.getKey(), entry.getValue());
				} catch (AlreadyPresentException | OnlyOneTrueException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
			}
			return true;
		}
		return false;
		
	}
	

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Card)) return false;
		Card c = (Card) obj;
		for (Question question: questions) {
			if (c.getQuestions().contains(question)) return true;
		}
		return false;
		
	}

	public List<Question> getQuestions()
	{
		List<Question> tmp = new ArrayList<>();
		for (Question question : questions) {
			tmp.add(question);
		}
		return tmp;
		
	}
	
	public Card clone()
	{
		Card tmp = new Card();
		List<Question> quest = getQuestions();
		for (Question question : quest) {
			try {
				tmp.addQuestion(question);
			} catch (AlreadyPresentException | CatAlreadyPresentException | SizeLimitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tmp;
	}
	
	public String toString()
	{
		String tmp =  "";
		for(int i = 0; i < questions.size(); i++)
		{
			tmp += "Question " + (i+1) + "\n" + questions.get(i).toString() + "\n\n";
		}
		return tmp;
	}
	
	/**
	 * Verifie si des tout les question de la carte sont valide 
	 * Une question est coonsideree comme valide si aucun de ses attribut n'est null
	 * @return true si toutes les question sont valide
	 */
	public boolean verify()
	{
		for (Question question : questions) {
			if(!question.verify()) return false;
		}
		return true;
	}
	
	
	
	
}

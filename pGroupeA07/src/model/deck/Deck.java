package model.deck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * classe de gestion de cartes
 * @author natha
 *
 */
public class Deck implements Serializable{
	private List<Card> cards;
	
	public Deck()
	{
		cards = new ArrayList<>();
	}
	
	/**
	 * ajoute une carte au deck
	 * @param c = la carte a ajouter
	 */
	public void addCard(Card c)
	{
		if (!(cards.contains(c)))
		{
			cards.add(c);
		}
	}
	
	/**
	 * supprime une carte du deck
	 * @param c la carte a supprimer
	 */
	public void deleteCard(Card c)
	{
		cards.remove(c);
	}
	
	public List<Card> getCards()
	{
		List<Card> tmp = new ArrayList<>();
		for (Card card : cards) {
			tmp.add(card.clone());
		}
		return  tmp;
	}
	
	
	public String toString()
	{
		String tmp="";
		for(int i = 0; i< cards.size(); i++)
		{
			tmp += "Card " + (i+1) + "\n" + cards.get(i).toString() + "\n\n";
		}
		return tmp;
	}
	
	/**
	 * Verifie si toutes les carte du deck sont valides
	 * Une carte est considérer comme valide si toute ses questions le sont également 
	 * @return true si toutes les cartes sont valides
	 */
	public boolean verify()
	{
		for (Card card : cards) {
			if(!card.verify()) return false;
		}
		return true;
	}
}

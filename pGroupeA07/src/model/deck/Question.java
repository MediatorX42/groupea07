package model.deck;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import enumeration.Category;
import exception.AlreadyPresentException;
import exception.OnlyOneTrueException;
/**
 * represente une question du jeux, avec sa liste de choix
 * @author natha
 *
 */
public class Question implements Serializable{
	private String author;
	private Category category;
	private String interrogation;
	private Map<String, Boolean> choices;
	
	public Question(String author, Category category, String interrogation) {
		this.author = author;
		this.category = category;
		this.interrogation = interrogation;
		choices = new HashMap<>();
	}
	
	/**
	 * Ajoute un choix a la question
	 * @param lib = libelle du choix
	 * @param isCorrect = boolean représentant si la question est correct ou non
	 * @throws AlreadyPresentException
	 * @throws OnlyOneTrueException
	 */
	public void addChoices(String lib, boolean isCorrect) throws AlreadyPresentException, OnlyOneTrueException
	{
		if(lib.equals("")) throw new AlreadyPresentException();
		if(choices.containsKey(lib)) throw new AlreadyPresentException();
		if(choices.containsValue(true) && isCorrect == true) throw new OnlyOneTrueException();
		
		choices.put(lib, isCorrect);
	}
	
	/**
	 * Supprime un choix de la question
	 * @param lib = libelle du choix 
	 */
	public void deleteChoices(String lib)
	{
		choices.remove(lib);
	}
	
	/**
	 * supprime tout les choix d'une question
	 */
	public void clearChoices()
	{
		choices = new HashMap<>();
	}

	@Override
	public int hashCode() {
		return Objects.hash(category, interrogation);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		return category == other.category && Objects.equals(interrogation, other.interrogation);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getInterrogation() {
		return interrogation;
	}

	public void setInterrogation(String interrogation) {
		this.interrogation = interrogation;
	}
	
	
	public Question clone()
	{
		Question q =new Question(author, category, interrogation);
		Set<Map.Entry<String, Boolean> > parcours = choices.entrySet();
		for (Map.Entry<String, Boolean> entry : parcours) {
			try {
				q.addChoices(entry.getKey(), entry.getValue());
			} catch (AlreadyPresentException | OnlyOneTrueException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return q;
	}
	
	public String toString()
	{
		String tmp= author+ "\n" + interrogation + "\n" + getCategory() + "\n";
		Set<Map.Entry<String, Boolean> > parcours = choices.entrySet();
		for (Map.Entry<String, Boolean> entry : parcours) {
			tmp += entry.getKey() + " ("+ entry.getValue() +")"+ "\n";
		}
		
		return tmp;
	}
	
	public Set<Map.Entry<String, Boolean> > getChoices()
	{
		return choices.entrySet();
	}
	
	/**
	 * verifie si une question est valide
	 * Une question est consideree comme valide si aucun de ses attribut n'est null
	 * @return true si la question est valide 
	 */
	public boolean verify()
	{
		if(author == null) return false;
		if(category == null) return false;
		if(interrogation == null || interrogation.trim().isEmpty()) return false;
		
		Set<Map.Entry<String, Boolean> > parcours = choices.entrySet();
		for (Map.Entry<String, Boolean> entry : parcours) {
			if(entry.getKey() == null) return false;
		}
		return true;
		
	}
	
	
	
	
}

package model.tiles;
/**
 * represente une case du plateau
 * @author natha
 *
 */
public abstract class Case {
	
	private double x;
	private double y;
	private int ind;

	public Case(double x, double y, int ind)
	{
		this.x = x;
		this.y = y;
		this.ind=ind;
	}
	
	public boolean equals(Object o)
	{
		if(!(o instanceof Case)) return false;
		Case c = (Case) o;
		return this.getX() == c.getX() && this.getY() == c.getY();
	}	

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getInd() {
		return ind;
	}
}

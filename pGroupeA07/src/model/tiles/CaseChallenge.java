package model.tiles;
/**
 * Represente une case de defis, redefinit Case
 * @author natha
 *
 */
public class CaseChallenge extends Case{
	private String challenge;
	
	public CaseChallenge(double x, double y, int ind, String challenge)
	{
		super(x, y, ind);
		this.challenge=challenge;
	}
	
	public CaseChallenge clone()
	{
		return new CaseChallenge(getX(), getY(), getInd(), challenge);
	}
	
	public String getChallenge()
	{
		return challenge;
	}
}

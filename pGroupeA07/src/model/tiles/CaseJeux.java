package model.tiles;

import enumeration.Category;
/**
 * Represente une case de jeu, redefinit Case
 * @author natha
 *
 */
public class CaseJeux extends Case{
	private Category cat;
	
	public CaseJeux(double x, double y, int ind, Category cat)
	{
		super(x, y, ind);
		this.cat=cat;
	}
	
	public Category getCat() {
		return cat;
	}
	
	public CaseJeux clone()
	{
		return new CaseJeux(getX(), getY(), getInd(), cat);
	}
}

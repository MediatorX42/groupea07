package model.tiles;

/**
 * contient les chemin vers les images de chaque tuiles
 * @author natha
 *
 */
public enum Tiles {
	IDEATILE("ideaTile.png"),
	SCIENCETILE("scienceTile.png"),
	PLANETTILE("planetTile.png"),
	HISTORYTILE("historyTile.png"),
	LITERATURETILE("literatureTile.png"),
	COMPUTERTILE("computerTile.png"),
	CHALLENGETILE("challengeTile.png");
	
	String s;
	Tiles(String s) {
		this.s = s;
	}
	public String getS() {
		return s;
	}
}

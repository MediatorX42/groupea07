package online;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import enumeration.Category;
import message.Message;
import message.Operation;
import model.GameManager;

/**
 * represente un client coté serveur, classe qui ecoute et envoie des message en tant que serveur
 * @author natha
 *
 */
public class ClientHandler implements Runnable, Serializable{

	public static ArrayList<ClientHandler> clientHandlers = new ArrayList<>();
	public static int indCurrentPlayer = -1;
	public static GameManager gm = new GameManager();
	
	private Socket socket;
	private ObjectOutputStream bufferedWriter;
	private ObjectInputStream bufferedReader;
	private Object clientUserName;
	private boolean ready = false;
	
	
	/**
	 * initialise un clientHandler
	 * @param socket = socket qui sera utiliser par le clientHandler
	 */
	public ClientHandler(Socket socket) {
		try {
			// Initialisation du socket, et creation des buffer  de lecture et ecriture
			this.socket = socket;
			this.bufferedWriter = new ObjectOutputStream(socket.getOutputStream());
			this.bufferedReader = new ObjectInputStream(socket.getInputStream());
			// Reception du message envoye par le client lors de sa connexion,
			// contenant son nom d'utilisateur
			Object object = bufferedReader.readObject();
			if(object instanceof Message) {
				Message m = (Message)object;
				clientUserName = m.getMissive();
			}
			// Ajout de l'objet a la liste de clientHandler
			clientHandlers.add(this);
			List<String> clientsName = new ArrayList<>();
			for (ClientHandler c : clientHandlers) {
				clientsName.add((String)c.clientUserName);
			}
			// Envoi a tous les clientHandler de l'information d'un nouveau joueur
			broadcastMessage(new Message(Operation.CONNECT, clientsName));
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			closeEverything(socket, bufferedReader, bufferedWriter);
		}
	}
	
	/**
	 * Lis les message recu par les client
	 */
	@Override
	public void run() {
		Message messageFromClient;
		while(socket.isConnected()) {
			try {
				// Reception du message envoye par le client
				messageFromClient = (Message)bufferedReader.readObject();
				// Decodage de l'operation contenue dans le message, afin d'envoyer aux autres 
				// utilisateurs l'information correspondante
				switch(messageFromClient.getOperation()) {
					case READY: this.ready = !ready;
						if(everyReady()) broadcastMessage(new Message(Operation.STARTGAME, null));
						break;
					case MESSAGE : broadcastMessageExceptSender(messageFromClient);
						break;
					case QUESTION_CATEGORY:broadcastMessage( new Message(Operation.QUESTION_CATEGORY, gm.randomQuestion((Category)messageFromClient.getMissive()))); 
						break;
					case DISCONNECT: deconnectClient((String)messageFromClient.getMissive());
						break;
					default: broadcastMessage(messageFromClient);
						break;
				}
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
				closeEverything(socket, bufferedReader, bufferedWriter);
				break;
			}
		}
	}
	
	
	/**
	 * Methode permettant de verifier que tout le monde a indique etre pret a commencer la partie
	 * @return
	 */
	public boolean everyReady() {
		for (ClientHandler clientHandler : clientHandlers) {
			if(!clientHandler.ready) return false;
		}
		return true;
	}
	
	/**
	 *  Methode permettant d'envoyer un message a tous les clients
	 * @param message = message a envoyer
	 */
	public void broadcastMessage(Message message) {
		for (ClientHandler clientHandler : clientHandlers) {
			try {
				clientHandler.bufferedWriter.writeObject(message);
				clientHandler.bufferedWriter.flush();
			} catch(IOException e) {
				e.printStackTrace();
				closeEverything(socket, bufferedReader, bufferedWriter);
			}
		}
	}
	
	/**
	 *  Methode permettant d'envoyer un message a tous les clients exepte celui selectionne
	 * @param message = message a envoyer
	 */
	public void broadcastMessageExceptSender(Message message) {
		for (ClientHandler clientHandler : clientHandlers) {
			if(clientHandler != this) {
				try {
					clientHandler.bufferedWriter.writeObject(message);
					clientHandler.bufferedWriter.flush();
				} catch(IOException e) {
					e.printStackTrace();
					closeEverything(socket, bufferedReader, bufferedWriter);
				}
			}
		}
	}
	
	/**
	 *  Methode retirant un client de la liste et envoyant l'information aux autres
	 */
	public void removeClientHandler() {
		clientHandlers.remove(this);
		broadcastMessage(new Message(Operation.DISCONNECT, clientUserName));
	}
	
	/**
	 * Enleve un lcient de la liste des clientHandler
	 * @param name = Nom du client a enlever
	 */
	private void deconnectClient(String name) {
		clientHandlers.remove(getClientHandlerFromName(name));
		List<String> clientsName = new ArrayList<>();
		for (ClientHandler c : clientHandlers) {
			clientsName.add((String)c.clientUserName);
		}
		broadcastMessage(new Message(Operation.DISCONNECT, clientsName));
	}
	
	/**
	 * Methode permettant de recuperer un client grace a son nom
	 * @param name = bom du client 
	 * @return ClientHandler le client recherchee
	 */
	public ClientHandler getClientHandlerFromName(String name)
	{
		for (ClientHandler clientHandler : clientHandlers) {
			if(clientHandler.getClientUserName().equals(name)) return clientHandler;
		}
		return null;
	}
	
	public Object getClientUserName() {
		return clientUserName;
	}
	
	/**
	 *  Methode permettant de cloturer le socket et les buffer du client
	 * @param socket 
	 * @param bufferedReader2
	 * @param bufferedWriter2
	 */
	public void closeEverything(Socket socket, ObjectInputStream bufferedReader2, ObjectOutputStream bufferedWriter2) {
		removeClientHandler();
		try {
			if(bufferedReader2 != null) {
				bufferedReader2.close();
			}
			if(bufferedWriter2 != null) {
				bufferedWriter2.close();
			}
			if(socket != null) {
				socket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

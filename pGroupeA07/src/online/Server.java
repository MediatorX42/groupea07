package online;


import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
/**
 * script du server, permet au joeurs de se connecter entre eux
 * @author natha
 *
 */
public class Server implements Serializable{

	private ServerSocket serverSocket;
	
	/**
	 *  Constructeur donnant le serverScket
	 * @param serverSocket = socket du server
	 */
	public Server(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	
	/**
	 *  Methode permettant de lancer le serveur, et d'attendre chaque client
	 */
	public void startServer() {
		try {
			while(!serverSocket.isClosed()) {
				// Nouveau socket pour chaque client se connectant
				Socket socket = serverSocket.accept();
				
				// Creation d'un ClientHandler avec le socket
				ClientHandler clientHandler = new ClientHandler(socket);
				
				// Lancement d'un nouveau Thread contenant le clientHandler
				Thread thread = new Thread(clientHandler);
				thread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  Methode permettant de fermer le serveur
	 */
	public void closeServerSocket() {
		try {
			if(serverSocket != null) {
				serverSocket.close();
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 *  Classe Main pour creer et demarrer le serveur
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(5000);
		Server server = new Server(serverSocket);
		server.startServer();
	}
}

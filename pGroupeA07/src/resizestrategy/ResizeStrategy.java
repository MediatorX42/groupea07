package resizestrategy;

import view.GameViewSP;

public interface ResizeStrategy {
	
	public void resizeRoot(GameViewSP gvSP);
}

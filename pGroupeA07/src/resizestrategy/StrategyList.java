package resizestrategy;

public enum StrategyList {
	W1024H576(1024, 576, "1024x576"),
	W1280H720(1280, 720, "1280x720"),
	W1600H900(1600, 900, "1600x900"),
	W1920H1080(1920, 1080, "1920x1080");
	
	
	double width;
	double height;
	String s;
	
	StrategyList(double width, double height, String s){
		this.width = width;
		this.height = height;
		this.s = s;
	}

	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public String getS() {
		return s;
	}
	
	public static StrategyList fromString(String s) {
		for (StrategyList strategy : StrategyList.values()) {
			if(strategy.getS().equals(s)) return strategy;
		}
		return null;
	}
	
	public static StrategyList fromResizeStrategy(ResizeStrategy rs)
	{
		for (StrategyList strategy : StrategyList.values()) {
			if(strategy.name().equals(rs.getClass().getSimpleName())) return strategy;
		}
		return null;
	}
}

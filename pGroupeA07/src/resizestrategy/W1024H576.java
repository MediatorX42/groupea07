package resizestrategy;

import view.GameViewSP;

public class W1024H576 implements ResizeStrategy {
	
	@Override
	public void resizeRoot(GameViewSP gvSP) {
		gvSP.getPrimaryStage().setWidth(1024);
		gvSP.getPrimaryStage().setHeight(576);
		gvSP.getPrimaryStage().centerOnScreen();
		gvSP.getStylesheets().clear();
		gvSP.getStylesheets().add(getClass().getResource("cssto1600.css").toExternalForm());
	}
}

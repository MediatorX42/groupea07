package resizestrategy;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import view.GameViewSP;

public class W1280H720 implements ResizeStrategy {
	
	@Override
	public void resizeRoot(GameViewSP gvSP) {
		gvSP.getPrimaryStage().setWidth(1280);
		gvSP.getPrimaryStage().setHeight(720);
		gvSP.getPrimaryStage().centerOnScreen();
		gvSP.getStylesheets().clear();
		gvSP.getStylesheets().add(getClass().getResource("cssto1600.css").toExternalForm());
	}
}

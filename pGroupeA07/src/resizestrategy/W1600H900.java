package resizestrategy;

import view.GameViewSP;

public class W1600H900 implements ResizeStrategy {
	
	@Override
	public void resizeRoot(GameViewSP gvSP) {
		gvSP.getPrimaryStage().setWidth(1600);
		gvSP.getPrimaryStage().setHeight(900);
		gvSP.getPrimaryStage().centerOnScreen();
		gvSP.getStylesheets().clear();
		gvSP.getStylesheets().add(getClass().getResource("cssto1920.css").toExternalForm());
	}

}

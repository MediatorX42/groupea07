package resizestrategy;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import view.GameViewSP;

public class W1920H1080 implements ResizeStrategy{
	
	@Override
	public void resizeRoot(GameViewSP gvSP) {
		gvSP.getPrimaryStage().setWidth(1920);
		gvSP.getPrimaryStage().setHeight(1080);
		gvSP.getPrimaryStage().centerOnScreen();
		gvSP.getStylesheets().clear();
		gvSP.getStylesheets().add(getClass().getResource("cssto1920.css").toExternalForm());
	}
}

package test;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connexion.User;
import connexion.UserManagement;
import exception.AlreadyPresentException;


class testUserManagement {
	private UserManagement userManagement;
	private User user1;
	private User user2;
	private List<User> list;
	@BeforeEach
	void setUp() throws Exception {
		userManagement = new UserManagement();
		user1 = new User("Paul", "123");
		user2 = new User("Paul", "123");
		Field f = userManagement.getClass().getDeclaredField("utilisateurs");
		f.setAccessible(true);
		list =(List<User>) f.get(userManagement);
	}

	@AfterEach
	void tearDown() throws Exception {
		userManagement = null;
		user1 = null;
		user2 = null;
		list = null;
	}

	@Test
	void testAjouter() throws AlreadyPresentException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		userManagement.ajouter(user1);
		assertEquals(1, list.size(), "La taille de la liste doit être de 1 après le premier ajout");
		assertTrue(list.contains(user1), "la liste doit contenir l'objet ajouter");
		assertThrows(AlreadyPresentException.class, ()->userManagement.ajouter(user2), "On ne peut pas ajuter 2 fois le même utilisateur");
		Field fld = user1.getClass().getDeclaredField("userName");
		fld.setAccessible(true);
		fld.set(user1, "Test");
		
		assertEquals("Paul", list.get(0).getUserName(), "La valeur ne doit pas avoir été modifiée");
	}

}

package view;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;

// Cette methode abstraite applique des ombres sur les nodes envoyes
public abstract class ApplyDropShadow {

	// Cette methode cree une ombre 
	public static DropShadow createDropShadow() {
		DropShadow ds = new DropShadow();
		ds.setOffsetY(10.0);
		ds.setColor(Color.BLACK);
		return ds;
	}
	
	// Cette methode applique l'ombre sur une liste de nodes envoyes en argument
	public static void applyDropShadow(ObservableList<Node> n) {
		for (Node node : n) {
			node.setEffect(createDropShadow());
		}
	}
	
	// Cette methode applique une ombre sur un node envoye en argument
	public static void applyDropShadow(Node n) {
		n.setEffect(createDropShadow());
	}
}

package view;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import view.ingame.GenerationPionAvatar;

// Cette classe permet de creer des ColorPicker
public class ColorPickerBP extends BorderPane {
	
	private static List<ColorPickerBP> colorPickerBPs = new ArrayList<ColorPickerBP>();

	private final double TAILLE = 15;
	private List<Rectangle> rectangles;
	private Rectangle selection;
	
	// Constructeur permettant de creer un colorPicker avec un indice
	public ColorPickerBP(int indDefaut){
		
		// Ajout a la liste des colorPicker
		colorPickerBPs.add(this);
		
		this.setPadding(new Insets(2));
		rectangles = new ArrayList<Rectangle>();
		
		// Creation de 56 rectangles representant toutes les couleurs, 
		// repartis dans des vbox et hbox afin d'avoir un quadrillage
		int k = 0;
		VBox vb = new VBox();
		for(int i = 0 ; i < 4 ; i++) {
			HBox hb = new HBox();
			for(int j = 0 ; j < 14 ; j++) {
				if(k == 56) break;
				
				// Creation du rectangle et definition des tailles
				Rectangle rect = new Rectangle();
				rect.setWidth(TAILLE);
				rect.setHeight(TAILLE);
				
				// Ajout d'un id qui est la valeur en String de la couleur du rectangle 
				// et remplissage du rectangle dans cette couleur
				rect.setId(NesColor.values()[k].getS());
				rect.setFill(Color.web(rect.getId()));
				rect.getStyleClass().add("rectColorPicker");
				// Ajout d'un evenement au rectangle, qui ajoute un effet de selection et l'applique a tous les colorPicker
				// grace a la methode setSelection
				rect.setOnMouseClicked(ev -> {
					if(!(rect.getEffect() instanceof InnerShadow)) {
						setSelection(rect);
						rect.setEffect(new InnerShadow());
					}
				});
				// Application de l'effet pour une case correspondant a l'indice du joueur
				if(k== indDefaut)
				{
					setSelection(rect);
					rect.setEffect(new InnerShadow());
					GenerationPionAvatar.generatePawnAvatar(Color.valueOf(rect.getId()), String.valueOf(k));
				}
				hb.getChildren().add(rect);
				rectangles.add(rect);
				k++;
			}
			vb.getChildren().add(hb);
		}
		
		// Pour chaque joueur, on ajout un effet aux autres colorPicker 
		for(int i = 0; i< indDefaut; i++)
		{
			rectangles.get(i).setEffect(new InnerShadow());
			rectangles.get(i).setDisable(true);
		}
		this.setCenter(vb);
		BorderPane.setAlignment(vb, Pos.CENTER);
	}
	
	public List<Rectangle> getRectangles() {
		return rectangles;
	}
	
	public static List<ColorPickerBP> getColorPickerBPs() {
		return colorPickerBPs;
	}
	
	public Rectangle getSelection() {
		if(selection != null) {
			return selection;
		} else {
			return rectangles.get(0);
		}
	}

	// Permet d'appliquer un effet sur tous les rectangles grace a updateColors()
	public void setSelection(Rectangle rect) {
		if(selection != null) {
			updateColors(selection.getId());
			selection.setEffect(null);
		}
		updateColors(rect.getId());
		selection = rect;
	}

	// Cette methode met a jour tous les colorPicker
	private void updateColors(String id) {
		// Pour tous les colorPicker
		for (ColorPickerBP colorPickerBP : colorPickerBPs) {
			// Pour tous les rectangles
			for (Rectangle rect : colorPickerBP.getRectangles()) {
				if(!colorPickerBP.equals(this)) {
					if(rect.getId().equals(id)) {
						// Desactive le rectangle de couleur choisi
						rect.setDisable(!rect.isDisable());
						// Ajoute l'effet de selection ou le retire 
						if(rect.isDisable()) {
							rect.setEffect(new InnerShadow());
						} else {
							rect.setEffect(null);
						}
					}
				}
			}
		}
	}
	
	// Cette methode permet de remplir un Rectangle dans la couleur choisie
	public void fillARect(Rectangle rect) {
		rect.setFill(Color.web(getSelection().getId()));
		rect.setId(getSelection().getId());
	}
}


package view;

import java.util.List;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.StackPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import media.MusicManager;
import model.GameManager;
import model.Player;
import resizestrategy.ResizeStrategy;
import resizestrategy.StrategyList;
import resizestrategy.W1024H576;
import resizestrategy.W1280H720;
import view.ingame.OnlineDisplaySP;
import view.ingame.PlayerPanelGP;
import view.ingame.QuestionDisplaySP;
import view.ingame.VictoryBP;
import view.menus.ConnexionBP;
import view.menus.GestionDeckAP;
import view.menus.InscriptionBP;
import view.menus.LocalGameBP;
import view.menus.MainMenuBP;
import view.menus.OnlineBP;
import view.menus.ParametersBP;

/**
 * La classe GameView va contenir tous les affichages necessaires au bon fonctionnement du jeu.
 * Elle cree les instances de chaque layout, et rend visible celui qui est actif selon la phase du jeu.
 *
 */
public class GameViewSP extends StackPane {
	
	private Stage primaryStage;
	private ResizeStrategy resizeStrategy;
	
	private InscriptionBP inscBP;
	private ConnexionBP coBP;
	private MainMenuBP mmBP;
	private QuestionDisplaySP qdBP;
	private LocalGameBP lgBP;
	private VictoryBP vBP;
	private ParametersBP paBP;
	private GestionDeckAP gdAP;
	private OnlineBP onBP;
	private OnlineDisplaySP onDiSP;
	
	private GameManager gameManager;
	
	//variable relative a la taille de l'ecran
	private double stageWidth;
	private double stageHeight;
	private boolean fullScreen;
	
	//variable relative a l'admin
	private boolean admin;	
	private boolean connected;
	private String usrName;
	
	private MusicManager musicManager;
	
	/**
	 * Le constructeur "rempli" le stackpane avec les layout crees, 
	 * puis les rends tous invisibles sauf le menu principal.
	 * Il place le stackPane au centre de la fenetre.
	 */
	public GameViewSP(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.gameManager = new GameManager();
		setResizeStrategy(new W1280H720());

		fullScreen = false;
		this.getChildren().addAll(getCoBP(), getInscBP(), getVBP());
		rendreInvisibleSauf(getCoBP());
		
		this.setBackground(new Background(new BackgroundImage(new Image("file:res/img/bckgnd2.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER	, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false))));
		this.musicManager = new MusicManager();
		musicManager.music2();
	}
	
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	
	public String getUsrName() {
		return usrName;
	}

	public MusicManager getMusicManager() {
		return musicManager;
	}

	public double getStageWidth() {
		return stageWidth;
	}
	
	public double getStageHeight() {
		return stageHeight;
	}
	
	public boolean isFullScreen() {
		return fullScreen;
	}
	
	public void setFullScreen(boolean fullScreen) {
		this.fullScreen = fullScreen;
		if(fullScreen)
		{
			stageWidth = Screen.getPrimary().getBounds().getWidth();
			stageHeight = Screen.getPrimary().getBounds().getHeight();
		}
	}
	
	public InscriptionBP getInscBP() {
		if(inscBP == null)
		{
			inscBP = new InscriptionBP(this);
		}
		return inscBP;
	}
	
	public ConnexionBP getCoBP() {
		if(coBP == null)
		{
			coBP = new ConnexionBP(this);
		}
		return coBP;
	}
	
	/**
	 * Getter utilise pour acceder au menu principal, layout en borderPane
	 * @return mmBP Interface de menu principal
	 */
	public MainMenuBP getMmBP() {
		if(mmBP == null) mmBP = new MainMenuBP(this);
		return mmBP;
	}
	/**
	 * Getter utilise pour acceder a l'affichage d'une question
	 * @return qdBP Interface d'affichage d'une question
	 */
	public QuestionDisplaySP getQdBP() {
		if(qdBP == null) qdBP = new QuestionDisplaySP(this);
		return qdBP;
	}

	/**
	 * Getter utilise pour acceder a l'interface de creation de jeu en local.
	 * @return lgBP Interface de jeu local
	 */
	public LocalGameBP getLgBP() {
		if(lgBP == null) lgBP = new LocalGameBP(this);
		return lgBP;
	}
	
	public VictoryBP getVBP() {
		if(vBP == null) vBP = new VictoryBP(this);
		return vBP;
	}

	public ParametersBP getPaBP() {
		if(paBP == null) paBP = new ParametersBP(this);
		return paBP;
	}

	public GestionDeckAP getGdAP() {
		if(gdAP == null) {
			gdAP = new GestionDeckAP(this);
		}
		return gdAP;
	}

	public OnlineBP getOnBP() {
		if(onBP == null) {
			onBP = new OnlineBP(this);
		}
		return onBP;
	}
	
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public OnlineDisplaySP getOnDiSP() {
		if(onDiSP == null) onDiSP = new OnlineDisplaySP(this);
		return onDiSP;
	}

	public void winningScreen(Player winningPlayer, List<Player> players)
	{
		vBP.setPlayer(winningPlayer, players);
		rendreInvisibleSauf(vBP);
	}
	
	/**
	 * Methode permettant de cacher tous les layouts presents dans le stackpane,
	 * sauf un specifie en argument
	 * @param node Noeud (layout) a garder visible
	 */
	public void rendreInvisibleSauf(Node node) {
		
		for (Node n : this.getChildren()) {
				n.setVisible(false);
		}
		node.setVisible(true);
	}

	public void resetGame() {
		PlayerPanelGP.resetNbPlayerPanel();
		this.getChildren().remove(getQdBP());
		this.getChildren().remove(getLgBP());
		this.getChildren().remove(getOnBP());
		this.onBP = null;
		this.lgBP = null;
		this.qdBP = null;
		rendreInvisibleSauf(getMmBP());
	}

	public void setResizeStrategy(ResizeStrategy resizeStrategy) {
		// Initialisation des attributs
		this.resizeStrategy = resizeStrategy;
		stageWidth = StrategyList.valueOf(resizeStrategy.getClass().getSimpleName()).getWidth();
		stageHeight = StrategyList.valueOf(resizeStrategy.getClass().getSimpleName()).getHeight();
		
		// Redimensionnement de la fenetre 
		this.resizeStrategy.resizeRoot(this);
	}
	
	public ResizeStrategy getResizeStrategy()
	{
		return resizeStrategy;
	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public boolean isAdmin() {
		return admin;
	}
	
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public GameManager getGameManager() {
		return gameManager;
	}
	
	//reset tout les nodes sauf le main menu
	public void resetAllNode()
	{
		qdBP = null;
		lgBP = null;
		gdAP = null;
		paBP = null;
		onBP = null;
	}
}

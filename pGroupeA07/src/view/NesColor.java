package view;

// Enumeration contenant toutes les couleurs utilisees dans le colorPicker
public enum NesColor {
	C28("f83800"),
	C13("0000bc"),
	C38("d8f878"),
	C43("58d854"),
	C24("e40058"),
	C50("00fcfc"),
	C42("b8f8b8"),
	C1("000000"),
	C2("fcfcfc"),
	C3("f8f8f8"),
	C4("bcbcbc"),
	C5("7c7c7c"),
	C6("a4e4fc"),
	C7("3cbcfc"),
	C8("0078f8"),
	C9("0000fc"),
	C10("b8b8f8"),
	C11("6888fc"),
	C12("0058f8"),
	C14("d8b8f8"),
	C15("9878f8"),
	C16("6844fc"),
	C17("4428bc"),
	C18("f8b8f8"),
	C19("f878f8"),
	C20("d800cc"),
	C21("940084"),
	C22("f8a4c0"),
	C23("f85898"),
	C25("a80020"),
	C26("f0d0b0"),
	C27("f87858"),
	C29("a81000"),
	C30("fce0a8"),
	C31("fca044"),
	C32("e45c10"),
	C33("881400"),
	C34("f8d878"),
	C35("f8b800"),
	C36("ac7c00"),
	C37("503000"),
	C39("b8f818"),
	C40("00b800"),
	C41("007800"),
	C44("00a800"),
	C45("006800"),
	C46("b8f8d8"),
	C47("58f898"),
	C48("00a844"),
	C49("005800"),
	C51("00e8d8"),
	C52("008888"),
	C53("004058"),
	C54("f8d8f8"),
	C55("787878"),
	C56("000000");

	String s;
	
	NesColor(String s) {
		this.s = s;
	}
	
	public String getS() {
		return s;
	}
}
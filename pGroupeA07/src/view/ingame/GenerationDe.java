package view.ingame;
	
import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SubScene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

// Cette classe va generer un de en 3 dimensions
public class GenerationDe {
	private static Timeline tl;
	private static final float SUBSCENE_HEIGHT = 100;
	private static final float SUBSCENE_WIDTH = 100;
	private static SubScene scene3d;
	
	private static Group groupDice = new Group();
	
	static float hw = 50 / 2f;
	static float hh = 50 / 2f;
	static float hd = 50 / 2f;
	
	static float points[] = {
		hw, hh, hd,
		hw, hh, -hd,
		hw, -hh, hd,
		hw, -hh, -hd,
		-hw, hh, hd,
		-hw, hh, -hd,
		-hw, -hh, hd,
		-hw, -hh, -hd,
	};
	static float x0 = 0f;
	static  float x1 = 1f / 4f;
	static float x2 = 2f / 4f;
	static float x3 = 3f / 4f;
	static float x4 = 1f;
	static float y0 = 0f;
	static float y1 = 1f / 3f;
	static float y2 = 2f / 3f;
	static float y3 = 1f;
    
    static float texCoords[] = {
        x1, y0,
        x2, y0,
        x0, y1,
        x1, y1,
        x2, y1,
        x3, y1,
        x4, y1,
        x0, y2,
        x1, y2,
        x2, y2,
        x3, y2,
        x4, y2,
        x1, y3,
        x2, y3
    };
    static int faces[] = {
    	0, 10, 2, 5, 1, 9,   //triangle A-C-B
	    2, 5, 3, 4, 1, 9,    //triangle C-D-B
	    4, 7, 5, 8, 6, 2,    //triangle E-F-G
	    6, 2, 5, 8, 7, 3,    //triangle G-F-H
	    0, 13, 1, 9, 4, 12,  //triangle A-B-E
	    4, 12, 1, 9, 5, 8,   //triangle E-B-F
	    2, 1, 6, 0, 3, 4,    //triangle C-G-D
	    3, 4, 6, 0, 7, 3,    //triangle D-G-H
	    0, 10, 4, 11, 2, 5,  //triangle A-E-C
	    2, 5, 4, 11, 6, 6,   //triangle C-E-G
	    1, 9, 3, 4, 5, 8,    //triangle B-D-F
	    5, 8, 3, 4, 7, 3     //triangle F-D-H
    };
    
    /**
     *  Cette methode permet de creer une subScene
     * @return
     */
	public static SubScene createScene3D() {
		if(scene3d == null) {
			scene3d = new SubScene(groupDice, SUBSCENE_WIDTH, SUBSCENE_HEIGHT);
		    groupDice.getChildren().addAll(cube());
		    groupDice.setTranslateX(50);
		    groupDice.setTranslateY(50);
		    scene3d.setCamera(new PerspectiveCamera());
		}
	    return scene3d;
	  }
	
	/**
	 *  Cette methode permet de creer un cube
	 * @return
	 */
	private static MeshView cube() {
		
		// Creation d'un mesh
		TriangleMesh mesh = new TriangleMesh();
		// Ajout de la matrice de point, des coordonnees et des faces
		mesh.getPoints().addAll(points);
		mesh.getTexCoords().addAll(texCoords);
		mesh.getFaces().addAll(faces);
		
		int faceSmoothingGroups[] = {
				0,0,1,1,2,2,3,3,4,4,5,5
		};
		mesh.getFaceSmoothingGroups().addAll(faceSmoothingGroups);
		
		// Chargement de l'image representant le de deplie, et ajout au materiel au cube
		Image diffuseMap = new Image(("file:res/dice/dice.png"));
		Material d1Material = new PhongMaterial(Color.WHITE, diffuseMap, null, null, null);
		MeshView cube = new MeshView(mesh);
		cube.setMaterial(d1Material);
		
		return cube;
	}
	
	/**
	 *  Permet de faire rouler le de, avec un effet aleatoire, en y applicant de nombreuses rotations
	 */
	public static void randomRollDice() {
		tl = new Timeline(new KeyFrame(Duration.seconds(0.01), ev -> {
			groupDice.getTransforms().addAll(
				new Rotate(new Random().nextInt(10)*10, Rotate.X_AXIS),
				new Rotate(new Random().nextInt(10)*10, Rotate.Y_AXIS),
				new Rotate(new Random().nextInt(10)*10, Rotate.Z_AXIS)
			);
		}));
		tl.setCycleCount(Timeline.INDEFINITE);
		tl.play();
	}
	
	/**
	 *  Permet d'arreter le de sur un numero precis recu en argument
	 * @param diceNumber
	 */
	public static void preciseNumberDice(int diceNumber) {
		tl.stop();
		groupDice.getTransforms().clear();
		switch(diceNumber) {
			case 1: break;
			case 2: groupDice.getTransforms().addAll(
						new Rotate(90, Rotate.Y_AXIS)
					);
					break;
			case 3: groupDice.getTransforms().addAll(
						new Rotate(90, Rotate.X_AXIS),
						new Rotate(90, Rotate.Y_AXIS)
					);
					break;
			case 4: groupDice.getTransforms().addAll(
						new Rotate(90, Rotate.Y_AXIS),
						new Rotate(-90, Rotate.Z_AXIS)
					);
					break;	
			case 5: groupDice.getTransforms().addAll(
						new Rotate(90, Rotate.X_AXIS),
						new Rotate(90, Rotate.Z_AXIS)
					);
					break;
			case 6: groupDice.getTransforms().addAll(
						new Rotate(180, Rotate.Y_AXIS)
					);
					break;
			default: break;
		}
	}
}

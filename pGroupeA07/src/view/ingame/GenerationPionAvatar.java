package view.ingame;
	
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import view.NesColor;

// Cette methode permet de generer des pions et avatars pour les joueurs, en fonction d'une couleur selectionnee
public class GenerationPionAvatar {
	private static final int NB_PLAYER_MAX = 8;
	private static List<Image> imagePawn = new ArrayList<>();
	private static List<Image> imageAvatar = new ArrayList<>();
	
	/**
	 *  Permet de recuperer toutes les images de pions
	 * @return
	 */
	public static List<Image> getImagePawn() {
		return imagePawn;
	}

	/**
	 *  Permet de recuperer toutes les images d'avatars
	 * @return
	 */
	public static List<Image> getImageAvatar() {
		return imageAvatar;
	}

	/**
	 *  Permet de generer les pions et avatars grace a la couleur choisir, et la position represente le numero du joueur
	 *  joueur 1 : position 1, joueur 2 : position 2, ...
	 * @param c1
	 * @param position
	 */
	public static void generatePawnAvatar(Color c1, String position) {
		
		Color c2 = c1.deriveColor(0, c1.getSaturation(), c1.getBrightness()/1.5, c1.getOpacity());
		imagePawn.add(Integer.valueOf(position) ,dessinerPion(c1, c2));
		imageAvatar.add(Integer.valueOf(position) , dessinerAvatar(c1, c2));
	}
	
	/**
	 *  Methode permettant de dessiner un pion
	 * @param clair
	 * @param sombre
	 * @return
	 */
	public static Image dessinerPion(Color clair, Color sombre) {
		Canvas canvas = new Canvas(40,56);
		GraphicsContext g = canvas.getGraphicsContext2D();
		
		g.save();
		// Contours
		g.setFill(Color.rgb(33, 33, 33));
		g.fillRect(8, 0, 24, 56);
		g.fillRect(0, 8, 40, 40);
		// Tete
		g.setFill(clair);
		g.fillRect(8, 8, 24, 8);
		// Corps
		g.setFill(sombre);
		g.fillRect(8, 24, 24, 24);
		g.restore();
		WritableImage writableImage = null;
        SnapshotParameters sp = new SnapshotParameters();
		sp.setFill(Color.TRANSPARENT);
		writableImage = new WritableImage((int)canvas.getWidth(), (int)canvas.getHeight());
		canvas.snapshot(sp, writableImage);
		return writableImage;
	}
	
	/**
	 *  Methode permettant de dessiner un avatar
	 * @param clair
	 * @param sombre
	 * @return
	 */
	public static Image dessinerAvatar(Color clair, Color sombre) {
		Canvas canvas = new Canvas(48,48);
		GraphicsContext g = canvas.getGraphicsContext2D();
		
		g.save();
		// Visage
		g.setFill(Color.web("bcaaa4"));
		g.fillRect(8, 8, 32, 16);
		g.fillRect(12, 8, 24, 28);
		// Corps
		g.setFill(clair);
		g.fillRect(0, 36, 48, 12);
		g.fillRect(4, 32, 16, 4);
		g.fillRect(28, 32, 16, 4);
		g.fillRect(12, 28, 4, 4);
		g.fillRect(32, 28, 4, 4);
		g.restore();

		// Cheveux
		g.setFill(Color.rgb(33, 33, 33));
		g.fillRect(16, 0, 16, 8);
		g.fillRect(12, 4, 4, 8);
		g.fillRect(8, 8, 4, 8);
		g.fillRect(32, 4, 4, 8);
		g.fillRect(36, 8, 4, 8);
		
		WritableImage writableImage = null;
        SnapshotParameters sp = new SnapshotParameters();
		sp.setFill(Color.TRANSPARENT);
		writableImage = new WritableImage((int)canvas.getWidth(), (int)canvas.getHeight());
		canvas.snapshot(sp, writableImage);
		return writableImage;
	}
	
	/**
	 *  Methode permetant de remplir la liste au depart, lorsqu'aucun joueur n'a encore choisi de couleur
	 */
	public static void fillList()
	{
		for(int i = 0; i<NB_PLAYER_MAX; i++)
		{
			Color c1 =Color.web(NesColor.values()[i].getS());
			Color c2 = c1.deriveColor(0, c1.getSaturation(), c1.getBrightness()/1.5, c1.getOpacity());
			imagePawn.add(i ,dessinerPion(c1, c2));
			imageAvatar.add(i , dessinerAvatar(c1, c2));
		}
	}
}

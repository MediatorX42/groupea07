package view.ingame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import model.GameManager;
import model.Player;
import model.deck.Question;
import model.tiles.CaseChallenge;
import view.ApplyDropShadow;
import view.GameViewSP;

public abstract class MainInGameViewSP extends StackPane{
	public static final int DECOMPTE_BECOMING_RED_AT = 3;
	//private StackPane gameWindowSP;
	private GameViewSP gvSP;
	private List<PlayerPanelGP> players;
	private int indCurrentPlayer = 0;
	
	private GameManager gm;
	private Label lblQuestion;
	private Label lblCategory;
	private Question currentQuestion;
	private Button btnQuit;
	private Button btnCheat;
	private VBox presentationQuestion;
	private PlateauSP plateauP;
	
	private VBox leftPlayersInfo;
	private VBox rightPlayersInfo;
	
	private SubScene sbsRoll;
	private Random r = new Random();
	
	private VBox vbQuitConfirmation;
	double translateX;
	double translateY;
	
	//variable du timer de la partie
	private int chronoHour, chronoMinut, chronoSecond;
	private Label lblBigTimer;
	private AnimationTimer animationBigTimer;
	
	//variable du timer par question
	private int decompte;
	private Label lblDecompte;
	private AnimationTimer animationDecompte;
	public static final int TIME_TO_ANSWER = 10;
	private VBox vbAnswers;
	
	public MainInGameViewSP(GameViewSP gvSP) {
		this.setId("questionDisplaySP");
		this.setBackground(new Background(new BackgroundImage(new Image("file:res/img/bckjeu.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER	, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false))));
		
		// Initialisations		
		this.gm = new GameManager();
		this.players = new ArrayList<>();
		this.gvSP = gvSP;
		
		
		this.getChildren().addAll(getPlateauP(), getLeftPlayersInfo(), getRightPlayersInfo(),getPresentationQuestion(), getBtnQuit(), getBtnCheat(), getVbQuitConfirmation(), getSbsRoll(), getLblBigTimer());
		displayPlayersInfo();
		getPlateauP().setVisible(true);
		getPlateauP().toBack();
		
		getPresentationQuestion().setVisible(false);
		StackPane.setAlignment(getBtnQuit(), Pos.BOTTOM_LEFT);
		StackPane.setAlignment(getBtnCheat(), Pos.BOTTOM_RIGHT);
		StackPane.setAlignment(getLblBigTimer(), Pos.TOP_CENTER);
		
		this.setOnScroll((ScrollEvent ev) ->{
			// Quand on zoom
			if(ev.getDeltaY() == 0) {
				ev.consume();
			}
			else if(ev.getDeltaY() > 0) {
				getPlateauP().setZoomIteration(1);
				getPlateauP().setScaleX(getPlateauP().getScaleX()+0.1);
				getPlateauP().setScaleY(getPlateauP().getScaleY()+0.1f);
				getPlateauP().setZoomed(true);
			} 
			// Si on dézoom
			else {
				// On regarde si ça a été zoomé auparavant, pour ne pas zoomer moins que la taille originale
				if(getPlateauP().getZoomIteration() > 0) {
					getPlateauP().setZoomIteration(-1);
					getPlateauP().setScaleX(getPlateauP().getScaleX()-0.1);
					getPlateauP().setScaleY(getPlateauP().getScaleY()-0.1);
				} else {
					getPlateauP().setZoomed(false);
				}
			}
		});
	}
	
	public Label getLblBigTimer() {
		if(lblBigTimer == null) {
			lblBigTimer = new Label("Time");
			lblBigTimer.getStyleClass().add("chrono");
			ApplyDropShadow.applyDropShadow(lblBigTimer);
			getAnimationBigTimer().start();
		}
		return lblBigTimer;
	}
	
	public AnimationTimer getAnimationBigTimer() {
		if(animationBigTimer == null) {
			animationBigTimer = new AnimationTimer() {
				long oldNow = 0;
				@Override
				public void handle(long now) {
					if(now - oldNow >= 1000000000) {
						oldNow = now;
						chronoSecond++;
						if(chronoSecond >= 60) {
							chronoSecond = 0;
							chronoMinut++;
						}
						if(chronoMinut >= 60) {
							chronoMinut = 0;
							chronoHour++;
						}
						
						getLblBigTimer().setText(String.format("%02d : %02d : %02d", chronoHour, chronoMinut, chronoSecond));
					}
				}
			};
		}
		return animationBigTimer;
	}
	
	public Label getLblDecompte() {
		if(lblDecompte == null)
		{
			lblDecompte = new Label();
			getLblDecompte().setStyle("-fx-font-size: 20");
		}
		return lblDecompte;
	}
	
	public AnimationTimer getAnimationDecompte() {
		if(animationDecompte == null)
		{
			animationDecompte = new AnimationTimer() {
				long old = 0;
				@Override
				public void handle(long now) {					
					if(now - old >= 1000000000) {
						old = now;
						decompte--;
						
						if(decompte <= DECOMPTE_BECOMING_RED_AT ){
							getLblDecompte().setStyle("-fx-text-fill: red; -fx-font-size: 20");
						}
						
						if(decompte < 0){
							nextTurn();
							animationDecompte.stop();
						}
						getLblDecompte().setText(String.valueOf(decompte));
					}
				}
			};
		}
		return animationDecompte;
	}
	
	public GameViewSP getGvSP() {
		return gvSP;
	}
	
	public VBox getLeftPlayersInfo() {
		if(leftPlayersInfo == null) {
			leftPlayersInfo = new VBox();
			leftPlayersInfo.setSpacing(40);
		}
		return leftPlayersInfo;
	}

	public VBox getRightPlayersInfo() {
		if(rightPlayersInfo == null) {
			rightPlayersInfo = new VBox();
			rightPlayersInfo.setSpacing(40);
		}
		return rightPlayersInfo;
	}
	
	public abstract SubScene getSbsRoll();
	
	public void movePlayer(int diceNumber) {
		Player tmp = getPlateauP().getPlayersList().get(getIndCurrentPlayer());
		Timeline moving = null;
		boolean endGameNeeded = false;
		//si le joueur ne finit pas un tour
		if(tmp.getIndCase() + diceNumber <= PlateauSP.MAX_IND_CASE)
		{
			
			
			moving = new Timeline(new KeyFrame(Duration.seconds(0.3), ev->{
				tmp.setIndCase(tmp.getIndCase()+1);
				getPlateauP().drawPlayerPawn();
			}));
			moving.setCycleCount(diceNumber);
			moving.play();
		}
		else
		{
			moving = new Timeline(new KeyFrame(Duration.seconds(0.3), ev->{
				if(tmp.getIndCase() + 1 <= PlateauSP.MAX_IND_CASE)
				{
					tmp.setIndCase(tmp.getIndCase()+1);
				}
				else
				{
					tmp.setIndCase(0);
				}
				
				getPlateauP().drawPlayerPawn();
			}));
			moving.setCycleCount(diceNumber);
			moving.play();
			
			//en cas de victoire
			if(getPlayers().get(getIndCurrentPlayer()).haveJustWin())
			{
				endGameNeeded = true;
				moving.setOnFinished(endGame->{
					//On envoie les joueurs sur l'écran de fin de partie
					List<Player> playersToSend = new ArrayList<>(); 
					for(PlayerPanelGP pp : getPlayers())
					{
						playersToSend.add(pp.getPlayer());
					}
					Timeline t = new Timeline(new KeyFrame(Duration.seconds(0.4), ev->{
						getGvSP().winningScreen(getPlayers().get(getIndCurrentPlayer()).getPlayer(), playersToSend);
					}));
					t.setCycleCount(1);
					t.play();
				});
				// Si le joueur remplit les conditions de victoire, alors on affiche l'ecran de victoire
			}
		}
		//TemplateMethode, les sous classe redéfinisse la suite de la méthode
		if(!endGameNeeded)
		{
			moveWhenNotEndGame(moving, tmp);
		}
	}
	
	public abstract void moveWhenNotEndGame(Timeline t, Player p);
	
	public PlateauSP getPlateauP() {
		if(plateauP == null) {
			plateauP = new PlateauSP(this);
			
			// Permettre de déplacer le plateau
			plateauP.setOnMousePressed(ev -> {
				translateX = ev.getX();
				translateY = ev.getY();
			});
			plateauP.setOnMouseDragged(ev -> {
				plateauP.setTranslateX(ev.getSceneX() - translateX);
				plateauP.setTranslateY(ev.getSceneY() - translateY);
			});
		}
		return plateauP;
	}

	public VBox getPresentationQuestion() {
		if(presentationQuestion == null) {
			presentationQuestion = new VBox();
			presentationQuestion.setPrefHeight(indCurrentPlayer);
		}
		return presentationQuestion;
	}
	
	public VBox getvbAnswers()
	{
		if(vbAnswers == null)
		{
			vbAnswers = new VBox();
		}
		return vbAnswers;
	}
	
	/**
	 * Cette méthode permet l'initialisation de la liste des PlayerPanel dont les noms correspondent aux noms entres dans LocalGame
	 * elle initialise également la liste des joueurs 
	 * @param playersName liste de noms de joueurs
	 */
	public void initializePlayer(List<String> playersName)
	{
		for (String name : playersName) {
			players.add(new PlayerPanelGP(name));
			getPlateauP().addPlayer(new Player(name));
		}
		
		//on dessine les joueurs sur le plateau après les avoir initialiser
		getPlateauP().drawPlayerPawn();
	}
	

	
	public abstract void displayChallenge(CaseChallenge c); //surement du code a recup ici
	
	/**
	 * Permet de changer le joueur qui doit repondre
	 */
	public void nextPlayer()
	{
		if(indCurrentPlayer >= players.size()-1) indCurrentPlayer = 0;
		else indCurrentPlayer++;
		//skip les joueurs qui doivent passer leurs tours
		while(players.get(indCurrentPlayer).getPlayer().isFrozen())
		{
			players.get(indCurrentPlayer).getPlayer().setFrozen(false);
		
			if(indCurrentPlayer >= players.size()-1) indCurrentPlayer = 0;
			else indCurrentPlayer++;
		}
	}
	
	/**
	 * Cette methode permet d'afficher pour chaque joueur un panneau d'informations
	 */
	public void displayPlayersInfo()
	{
		if(players.size()>0) {
			// Si plusieurs joueurs jouent
			if(players.size() > 1) {
				
				// Affichage des informations des joueurs
				for(int i = 0; i < players.size(); i++) {
					// Chaque joueur est reparti suivant sa place dans la liste / pair a gauche et impair a droite
					if (i%2 == 0)
					{
						getLeftPlayersInfo().getChildren().add(players.get(i));
					}
					else
					{	
						getRightPlayersInfo().getChildren().add(players.get(i));
					}
				}
				// Si un seul joueur participe a la partie, on cree un panneau invisible, qui maintiendra
				// une symetrie dans l'ecran de jeu
			} else {
				getLeftPlayersInfo().getChildren().add(players.get(0));
				PlayerPanelGP ppgp = new PlayerPanelGP("");
				ppgp.setId("invisible");
				getRightPlayersInfo().getChildren().add(ppgp);
			}
			//on les place en arrière plans pour que le bouton roll soit cliquable
			getLeftPlayersInfo().setMaxWidth(200);
			getLeftPlayersInfo().setAlignment(Pos.CENTER_LEFT);
			getLeftPlayersInfo().setMaxHeight(100);
			StackPane.setAlignment(getLeftPlayersInfo(), Pos.CENTER_LEFT);
			
			getRightPlayersInfo().setMaxWidth(200);
			getRightPlayersInfo().setAlignment(Pos.CENTER_RIGHT);
			getRightPlayersInfo().setMaxHeight(100);
			StackPane.setAlignment(getRightPlayersInfo(), Pos.CENTER_RIGHT);
			getRightPlayersInfo().toFront();
			activePlayer();
		}	
	}
	
	public abstract void nextTurn();
	
	/**
	 * Cette methode permet de creer le bouton Return avec un event appelant la methode resetGame() du GameViewSP
	 * @return btnQuit Bouton pour quitter
	 */
	public Button getBtnQuit() {
		if(btnQuit == null) {
			btnQuit = new Button("Quit");
			
			btnQuit.setOnAction(ev -> {
				getVbQuitConfirmation().setVisible(true);
				getVbQuitConfirmation().toFront();
			});
			btnQuit.setAlignment(Pos.BASELINE_CENTER);
			btnQuit.getStyleClass().add("btnQuitGameDisplay");
			btnQuit.toFront();
		}
		return btnQuit;
	}
	
	public VBox getVbQuitConfirmation() {
		if(vbQuitConfirmation == null) {
			vbQuitConfirmation = new VBox(new Label("Do you really want to leave this game?"));
			Button btnYes = new Button("Yes");
			Button btnCancel = new Button("Cancel");
			btnYes.getStyleClass().add("btnAnswer");
			btnCancel.getStyleClass().add("btnAnswer");
			btnYes.setOnAction(ev -> {
				gvSP.resetGame();
				
			});
			btnCancel.setOnAction(ev -> {
				vbQuitConfirmation.setVisible(false);
			});
			HBox hb = new HBox(btnYes, btnCancel);
			hb.setAlignment(Pos.CENTER);
			vbQuitConfirmation.getChildren().add(hb);
			vbQuitConfirmation.setVisible(false);
			vbQuitConfirmation.setAlignment(Pos.CENTER);
			vbQuitConfirmation.getStyleClass().add("displayQuestion");
			StackPane.setAlignment(vbQuitConfirmation, getAlignment());
		}
		return vbQuitConfirmation;
	}
	
	public void activePlayer() {
		for(int i = 0; i < players.size(); i++) {
			// Met en evidence le joueur qui doit repondre
			if(i == indCurrentPlayer)
			{
				DropShadow ds = new DropShadow();
				ds.setOffsetY(10.0);
				ds.setColor(Color.web("#212121"));
				players.get(i).setEffect(ds);
				
				players.get(i).getStyleClass().remove("otherPlayer");
				players.get(i).getStyleClass().remove("currentPlayer");
				players.get(i).getStyleClass().add("currentPlayer");
			}
			else
			{
				if(players.get(i).getEffect() instanceof DropShadow) {
					 players.get(i).setEffect(null);
				 }
				players.get(i).getStyleClass().remove("otherPlayer");
				players.get(i).getStyleClass().remove("currentPlayer");
				players.get(i).getStyleClass().add("otherPlayer");
			}
		}
	}
	
	//Boutton pour visualiser la fin d'une partie lors de la démonstration
	public Button getBtnCheat() {
		if(btnCheat == null)
		{
			btnCheat = new Button("EndGame");
			btnCheat.setOnAction(ev->{
				List<Player> playersToSend = new ArrayList<>(); 
				for(PlayerPanelGP pp : players)
				{
					playersToSend.add(pp.getPlayer());
				}
				gvSP.winningScreen(players.get(indCurrentPlayer).getPlayer(), playersToSend);
			});
			btnCheat.setAlignment(Pos.BASELINE_CENTER);
			btnCheat.toFront();
		}
		return btnCheat;
	}
	
	public void resetDecompte()
	{
		//reset du decompte
		decompte = TIME_TO_ANSWER + 1;
		getLblDecompte().setStyle("-fx-font-size: 20");
		getAnimationDecompte().start();
	}
	
	public GameManager getGm() {
		return gm;
	}
	
	public Label getLblQuestion() {
		return lblQuestion;
	}
	
	public void setLblQuestion(Label lblQuestion) {
		this.lblQuestion = lblQuestion;
	}
	
	public Label getLblCategory() {
		return lblCategory;
	}
	
	public void setLblCategory(Label lblCategory) {
		this.lblCategory = lblCategory;
	}
	
	public Question getCurrentQuestion() {
		return currentQuestion;
	}
	
	public void setCurrentQuestion(Question currentQuestion) {
		this.currentQuestion = currentQuestion;
	}
	
	public int getIndCurrentPlayer() {
		return indCurrentPlayer;
	}
	
	public void setIndCurrentPlayer(int indCurrentPlayer) {
		this.indCurrentPlayer = indCurrentPlayer;
	}
	
	public List<PlayerPanelGP> getPlayers() {
		return players;
	}
	
	public void clearVbAnswers()
	{
		vbAnswers = new VBox();
	}
	
	
}

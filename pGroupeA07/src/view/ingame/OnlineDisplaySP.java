package view.ingame;

import java.util.Map;
import java.util.Random;
import java.util.Set;

import enumeration.Category;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.util.Duration;
import message.Message;
import message.Operation;
import model.Player;
import model.deck.Question;
import model.tiles.Case;
import model.tiles.CaseChallenge;
import model.tiles.CaseJeux;
import view.GameViewSP;

public class OnlineDisplaySP extends MainInGameViewSP{
	private SubScene sbsRoll;
	Random r = new Random();

	public OnlineDisplaySP(GameViewSP gvSP) {
		super(gvSP);
	}

	public SubScene getSbsRoll() {
		if (sbsRoll == null)
		{
			sbsRoll = GenerationDe.createScene3D();
			StackPane.setAlignment(sbsRoll, Pos.BOTTOM_CENTER);
			Label lblRollRemplacement = new Label("Roll");
			this.getChildren().add(lblRollRemplacement);
			StackPane.setAlignment(lblRollRemplacement, Pos.BOTTOM_CENTER);
			lblRollRemplacement.toBack();
			sbsRoll.getStyleClass().add("btnRoll");
			lblRollRemplacement.setPadding(new Insets(20));
			lblRollRemplacement.setTextFill(Color.WHITE);
			sbsRoll.setOnMouseClicked(ev->{
				int diceNumber = 1 + r.nextInt(6);
				getGvSP().getOnBP().sendMessage(new Message(Operation.ROLL, diceNumber));
				
			});
			sbsRoll.setVisible(true);
		}
		return sbsRoll;
	}

	@Override
	public void moveWhenNotEndGame(Timeline moving, Player p) {
		moving.setOnFinished(questEvent->{
			Case c = getPlateauP().rechercherCase(p.getIndCase());
			Category cat = null;
			if(c instanceof CaseJeux) {
				cat = ((CaseJeux)c).getCat();
				if((getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName())))
					getGvSP().getOnBP().sendMessage(new Message(Operation.QUESTION_CATEGORY, cat));
			} else {
				displayChallenge((CaseChallenge) c);
			}
			getSbsRoll().setDisable(true);
			getSbsRoll().setVisible(false);
			Timeline t1 = new Timeline(new KeyFrame(Duration.seconds(0.5), ev->{
				for (Node node : this.getChildren()) {
					node.setEffect(new GaussianBlur());
				}
				getPresentationQuestion().setEffect(null);
				getPresentationQuestion().setVisible(true);
			}));
			
			t1.setCycleCount(1);
			t1.play();
		});
	}
	
	/**
	 * Cette methode permet de regenerer l'affichage global, et de proposer une nouvelle question.
	 */
	public void changeQuestion(Question currentQuestion)
	{
		resetDecompte();
		// Creation du vbTopOfBp qui servira de conteneur pour les informations
		getPresentationQuestion().getStyleClass().add("displayQuestion");

		// Initialisation de la question
		setCurrentQuestion(currentQuestion);
		
		// Ajout de la categorie de la question
		setLblCategory(new Label(getCurrentQuestion().getCategory().name()));
		
		getLblCategory().setStyle("-fx-text-fill:"+getCurrentQuestion().getCategory().getColor());
		getLblCategory().setTextAlignment(TextAlignment.CENTER);
		
		// Ajout de l'intitule de la question
		
		setLblQuestion(new Label(getCurrentQuestion().getInterrogation())); 
		
		getLblQuestion().setWrapText(true);
		TextFlow tmp = new TextFlow(getLblQuestion());
		getLblQuestion().setTextAlignment(TextAlignment.CENTER);
		getLblQuestion().setAlignment(Pos.TOP_CENTER);

		VBox labels = new VBox(getLblDecompte(), getLblCategory(), tmp);
		getLblQuestion().prefWidthProperty().bind(labels.widthProperty());
		//TextFlow tmp = new TextFlow(lblQuestion);
		labels.setMaxHeight(500);
		labels.setAlignment(Pos.TOP_CENTER);
		
		// Ajout de l'interrogation et de la categorie au VBox, et placement de celui-ci au top du BorderPane
		getPresentationQuestion().getChildren().clear();
		getPresentationQuestion().getChildren().addAll(labels);
		getPresentationQuestion().setAlignment(Pos.TOP_CENTER);
		getPresentationQuestion().setSpacing(100);
		
		// On passe sur chaque choix de la question obtenue aleatoirement, et on ajoute un boutton correspondant a ce choix
		// On cree un VBox qui sera place au centre du BorderPane
		Set<Map.Entry<String, Boolean> > choices = currentQuestion.getChoices();
		clearVbAnswers();
		getvbAnswers().setSpacing(10);
		
		for (Map.Entry<String, Boolean> entry : choices) {
		
			// Creation de chaque bouton contenant les choix de reponses a la question choisie
			Button btnAnswer = new Button(entry.getKey());
			btnAnswer.getStyleClass().add("btnAnswer");
			btnAnswer.setWrapText(true);
			getvbAnswers().getChildren().add(btnAnswer);
			
			// Pour chaque bouton, on ajoute un evenement qui se declenche lorsqu'une reponse est choisie par l'utilisateur
			if(getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName()))
			{
				btnAnswer.setOnAction(new EventHandler<ActionEvent>() {
					
					@Override
					public void handle(ActionEvent event) {
						if(entry.getValue())
						{
							getGvSP().getOnBP().sendMessage(new Message(Operation.ON_TRUE_ANSWER, entry.getKey()));
						}
						else
						{
							getGvSP().getOnBP().sendMessage(new Message(Operation.ON_FALSE_ANSWER, entry.getKey()));
						}
						
						// On desactive tous les boutons pour eviter que plusieurs choix soient faits
						for (Node n : getvbAnswers().getChildren()) {
							((Button)n).setStyle(null);
							n.setDisable(true);
						}						
					}
				});
			}

		}
		// Placement du VBox contenant les questions
		getPresentationQuestion().getChildren().add(getvbAnswers());
		getPresentationQuestion().toFront();
		getvbAnswers().setAlignment(Pos.BOTTOM_CENTER);
	}
	
	public void displayChallenge(CaseChallenge c)
	{
		//le clear la vbox de présentation
		getPresentationQuestion().getStyleClass().add("displayQuestion");
		getPresentationQuestion().getChildren().clear();
		setLblQuestion(new Label(c.getChallenge()));
		setLblCategory(new Label("CHALLENGE"));
		
		getLblQuestion().setAlignment(Pos.CENTER);
		getLblCategory().setAlignment(Pos.CENTER);
		
		VBox labels = new VBox(getLblCategory(), getLblQuestion());
		labels.setAlignment(Pos.TOP_CENTER);
		
		Timeline t = new Timeline(new KeyFrame(Duration.seconds(1.5), ev->{
			switch(c.getChallenge())
			{
				case "Skip your turn !" : 
					//je met le boolean frozen du joueur a true 
					getPlayers().get(getIndCurrentPlayer()).getPlayer().setFrozen(true);
					nextTurn();
					break;
					
				case "You just got teleported !":
					//je teste sur laquelle des 2 case le joueur est
					if(c.getInd() == 8)
					{
						//je change son indice dans la liste des QuesitonDisplay ainsi que dans la liste du plateau
						getPlayers().get(getIndCurrentPlayer()).getPlayer().setIndCase(24);
						getPlateauP().getPlayersList().get(getIndCurrentPlayer()).setIndCase(24);
						//je redessine les pions pour actualiser l'affichage 
						getPlateauP().drawPlayerPawn();
					}
					else
					{
						getPlayers().get(getIndCurrentPlayer()).getPlayer().setIndCase(8);
						getPlateauP().getPlayersList().get(getIndCurrentPlayer()).setIndCase(8);
						getPlateauP().drawPlayerPawn();
					}
					nextTurn();
					break;
					
				case "Random Question !":
					
					if(!getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName()))
					{
						int randNum = r.nextInt(6);
						getGvSP().getOnBP().sendMessage(new Message(Operation.QUESTION_CATEGORY, Category.values()[randNum]));
					}
					
					break;
					
				case "Everyone lose a category !":
					//je genere une categorie au hasard
					if(getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName()))
					{
						int numCat = r.nextInt(6);
						getGvSP().getOnBP().sendMessage(new Message(Operation.MALUS_CATEGORY, Category.values()[numCat]));
					}
					
					break;
					
				case "You just received a free Category !":
					//je parcourt la liste des categorie du joueurs en cours
					if(getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName()))
					{
						int numCat =  r.nextInt(6);
						while(getPlayers().get(getIndCurrentPlayer()).getPlayer().havevalidateCat(Category.values()[numCat]))
						{
							numCat =  r.nextInt(6);
						}
						getGvSP().getOnBP().sendMessage(new Message(Operation.BONUS_CATEGORY, Category.values()[numCat]));
						//players.get(indCurrentPlayer).validateCat(Category.values()[numCat]);
						
						
					}
					
					
					break;
				default : nextTurn();
					break;
			}
		}));
		t.setCycleCount(1);
		t.play();
		getPresentationQuestion().getChildren().addAll(labels);
		getPresentationQuestion().setAlignment(Pos.CENTER);
		
	}
	
	/**
	 * Cette methode appelle les differentes methodes permettant de continuer la partie
	 */
	public void nextTurn()
	{
		nextPlayer();
		
		if(!getGvSP().getOnBP().getUserName().equals(getPlayers().get(getIndCurrentPlayer()).getPlayer().getName()))
		{
			getSbsRoll().setDisable(true);
		}
		else
		{
			getSbsRoll().setDisable(false);
		}
		
		Label lb = new Label(getPlayers().get(getIndCurrentPlayer()).getLblName().getText() + ", it's your turn !");
		HBox hb = new HBox(lb);
		hb.setVisible(true);
		hb.getStyleClass().add("diplayPlayerTurn");
		this.getChildren().add(hb);
		hb.setAlignment(Pos.CENTER);
		lb.setTextAlignment(TextAlignment.CENTER);
		lb.setAlignment(getAlignment());
		Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1.3), ev -> {
			hb.setVisible(false);
		}));
		tl.setCycleCount(1);
		tl.play();
		for (Node node : this.getChildren()) {
			node.setEffect(null);
		}
		getPresentationQuestion().setVisible(false);
		activePlayer();
		getSbsRoll().setVisible(true);
	}

	/**
	 * Fait tourner le dé et l'arrete sur le chiffre passee en argument
	 * @param diceNumber
	 */
	public void rollDice(int diceNumber)
	{
		GenerationDe.randomRollDice();
		sbsRoll.setDisable(true);
		
		Timeline tl = new Timeline(new KeyFrame(Duration.seconds(2), event ->{
			GenerationDe.preciseNumberDice(diceNumber);
		}));
		tl.setOnFinished(event -> {
			Timeline tl2 = new Timeline(new KeyFrame(Duration.seconds(1), event2 -> {
				movePlayer(diceNumber);
			}));
			tl2.setCycleCount(1);
			tl2.play();
		});
		tl.setCycleCount(1);
		tl.play();
	}
	
	public void validatePlayerAnswer()
	{
		getPlayers().get(getIndCurrentPlayer()).validateCat(getCurrentQuestion().getCategory());
		getPlayers().get(getIndCurrentPlayer()).updatePlayerScore();
	}
	
	public void givePlayerACategorie(Category c)
	{
		getPlayers().get(getIndCurrentPlayer()).validateCat(c);
		nextTurn();
		
	}
	public void removeEveryoneACAtegory(Category c)
	{
		for (PlayerPanelGP playerPanelGP : getPlayers()) {
			playerPanelGP.removeCat(c);
		}
	
		nextTurn();
		
	}
}

//FAIRE CASE A PARTIR DE 5
package view.ingame;

import java.util.ArrayList;
import java.util.List;

import enumeration.Category;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;

import model.Player;
import model.tiles.Case;
import model.tiles.CaseChallenge;
import model.tiles.CaseJeux;
import model.tiles.Tiles;

public class PlateauSP  extends StackPane{
	private Canvas plateCanvas;
	private Canvas playerCanvas;
	private GraphicsContext plateContext;
	private GraphicsContext playerContext;
	
	private List<Case> cases;
	private List<Player> players;
	
	public static final int MAX_IND_CASE = 31;
	public static final double TILE_WIDTH = 110;
	public static final double TILE_HEIGHT = 55;	
	
	public static double WINDOW_WIDTH;
	public static double WINDOW_HEIGHT;
	public static final double IMG_WIDTH = 15;
	public static final double IMG_HEIGHT = 21;
	public static final Color CHALLENGE_COLOR = Color.GRAY;
	public static final Category[] categories = Category.values();
	public static final double[][] PAWN_OFFSET= {
			{0, 0, TILE_WIDTH/4, -TILE_WIDTH/4, TILE_WIDTH/8, -TILE_WIDTH/8, TILE_WIDTH/8, -TILE_WIDTH/8},
			{TILE_HEIGHT/4, TILE_HEIGHT - TILE_HEIGHT/4, TILE_HEIGHT/2, TILE_HEIGHT/2, TILE_HEIGHT/3, TILE_HEIGHT/3, TILE_HEIGHT - TILE_HEIGHT/3, TILE_HEIGHT- TILE_HEIGHT /3}
	};
	public static final String[] CHALLENGE_TAB = {"Everyone lose a category !", "Skip your turn !", "Random Question !", "You just got teleported !",
													"You just received a free Category !", "Hmm.. Nothings happened",};
	
	private boolean zoomed = false;
	private int zoomIteration = 0;
	
	
	
	public PlateauSP(MainInGameViewSP qdBp)
	{
		
		//initialisation
		cases = new ArrayList<>();
		players = new ArrayList<>();
		
		//initialisation de la taille de la fenetre 
		
		if(qdBp.getGvSP().isFullScreen())
		{
			WINDOW_WIDTH = Screen.getPrimary().getBounds().getWidth();
			WINDOW_HEIGHT = Screen.getPrimary().getBounds().getHeight();
		}
		else
		{
			WINDOW_WIDTH = qdBp.getGvSP().getStageWidth();
			WINDOW_HEIGHT = qdBp.getGvSP().getStageHeight();
		}
		
		Group group = new Group();
		plateCanvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
		playerCanvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
		
		group.getChildren().addAll(plateCanvas, playerCanvas);
		
		plateContext = plateCanvas.getGraphicsContext2D();
		playerContext = playerCanvas.getGraphicsContext2D();
		
		this.getChildren().addAll(plateCanvas, playerCanvas);
		
		drawPlate();
	}
	
	public PlateauSP(OnlineDisplaySP onlineDisplaySP) {
		//initialisation
		cases = new ArrayList<>();
		players = new ArrayList<>();
		
		//initialisation de la taille de la fenetre 
		
		if(onlineDisplaySP.getGvSP().isFullScreen())
		{
			WINDOW_WIDTH = Screen.getPrimary().getBounds().getWidth();
			WINDOW_HEIGHT = Screen.getPrimary().getBounds().getHeight();
		}
		else
		{
			WINDOW_WIDTH = onlineDisplaySP.getGvSP().getStageWidth();
			WINDOW_HEIGHT = onlineDisplaySP.getGvSP().getStageHeight();
		}
		
		Group group = new Group();
		plateCanvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
		playerCanvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
		
		group.getChildren().addAll(plateCanvas, playerCanvas);
		
		plateContext = plateCanvas.getGraphicsContext2D();
		playerContext = playerCanvas.getGraphicsContext2D();
		
		this.getChildren().addAll(plateCanvas, playerCanvas);
		
		drawPlate();
	}

	public int getZoomIteration() {
		return zoomIteration;
	}

	public void setZoomIteration(int zoomIteration) {
		this.zoomIteration += zoomIteration;
		if(this.zoomIteration == 0) {
			setZoomed(false);
		} else {
			setZoomed(true);
		}
	}

	public boolean isZoomed() {
		return zoomed;
	}
	
	public void setZoomed(boolean zoomed) {
		this.zoomed = zoomed;
	}
	/**
	 * Methode qui dessine une Tile sur le canvas du plateau
	 * @param x position en X
	 * @param y position en Y
	 * @param color couleur de la case (correspond a sa cat�gorie
	 */
	//
	public void drawTile(double x, double y, String tilePath)
	{
		plateContext.save();
		plateContext.translate(((((x - y) * TILE_WIDTH / 2) + TILE_WIDTH*2 + ((WINDOW_WIDTH - (TILE_WIDTH*9)) / 2)) + 50), 
				((x + y) * TILE_HEIGHT / 2) + ((WINDOW_HEIGHT - (TILE_HEIGHT*5.5)) / 2)+50);
		
		plateContext.beginPath();
		plateContext.moveTo(0, 0);
		plateContext.lineTo(TILE_WIDTH / 2, TILE_HEIGHT / 2);
		plateContext.lineTo(0, TILE_HEIGHT);
		plateContext.lineTo(-TILE_WIDTH / 2, TILE_HEIGHT / 2);
		plateContext.closePath();
		plateContext.setFill(Color.BLACK);
		GaussianBlur boxBlur = new GaussianBlur();
		boxBlur.setRadius(20);
		plateContext.setEffect(boxBlur);
		plateContext.fill();
		plateContext.restore();
		
		Image img = new Image("file:res/tiles2/" + tilePath);
		plateContext.save();
		plateContext.translate(((x - y) * TILE_WIDTH / 2) + TILE_WIDTH*2 + ((WINDOW_WIDTH - (TILE_WIDTH*9)) / 2), 
				((x + y) * TILE_HEIGHT / 2) + ((WINDOW_HEIGHT - (TILE_HEIGHT*5.5)) / 2));
		plateContext.drawImage(img, (0), 0, img.getWidth(), img.getHeight(), 0, 0, TILE_WIDTH, TILE_HEIGHT*1.5);
		
		plateContext.restore();
		
		
	
	}
	
	/**
	 * M�thode complexe qui dessine le plateau
	 */
	private void drawPlate()
	{
		//plateContext.clearRect(0, 0, plateCanvas.getWidth(), plateCanvas.getHeight());
		//Dessin du plateau
		//1er Segment
		double x=0, y=0;
		int i;
		//ajout caseChallenge
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 20, CHALLENGE_TAB[0])).clone());
		

		//ajout Case de jeux
		x++;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x+i, y, Tiles.values()[categories.length/2 -1 -i].getS());
			cases.add((new CaseJeux(x+i, y, 19-i, categories[categories.length/2 -1 -i])).clone());
			
		}
		
		//2eme Segment
		x=4;
		y=-4;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 4, CHALLENGE_TAB[1])).clone());
		
		y++;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x, y+i, Tiles.values()[categories.length/2 -1 -i].getS());
			cases.add((new CaseJeux(x, y+i, 3-i,categories[categories.length/2 -1 -i] )).clone());
			
		}
		y+=i;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		//case centrale
		cases.add((new CaseChallenge(x, y, 0, CHALLENGE_TAB[2])).clone());
		cases.add((new CaseChallenge(x, y, 16, CHALLENGE_TAB[2])).clone());
		
		//3emeSegment
		x=5;
		y=-4;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x+i, y, Tiles.values()[categories.length/2 +i].getS());
			cases.add((new CaseJeux(x+i, y, 5+i, categories[categories.length/2 +i])).clone());
			
		}
		x+=i;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 8, CHALLENGE_TAB[3])).clone());
		
		
		//4eme Segment
		x=0;
		y=1;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x, y+i, Tiles.values()[categories.length/2 +i].getS());
			cases.add((new CaseJeux(x, y+i, 21+i, categories[categories.length/2 +i])).clone());
			
		}
		y+=i;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 24, CHALLENGE_TAB[3])).clone());
		
		//5eme Segment
		x=4;
		y=1;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x, y+i, Tiles.values()[categories.length-1 -i].getS());
			cases.add((new CaseJeux(x, y+i, 31-i, categories[categories.length-1 -i])).clone());
			
		}
		
		//6eme Segment
		x=1;
		y=4;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x+i, y, Tiles.values()[i].getS());
			cases.add((new CaseJeux(x+i, y, 25+i, categories[i])).clone());
			
		}
		x+=i;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 28, CHALLENGE_TAB[4])).clone());
		
		//7eme Segment
		x=8;
		y=-3;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x, y+i, Tiles.values()[i].getS());
			cases.add((new CaseJeux(x, y+i, 9+i, categories[i])).clone());
			
		}
		//8eme Segment
		x=5;
		y=0;
		for(i = 0; i<categories.length/2; i++)
		{
			drawTile(x+i, y, Tiles.values()[categories.length-1 -i].getS());
			cases.add((new CaseJeux(x+i, y, 15-i, categories[categories.length-1 -i])).clone());
			
		}
		x+=i;
		drawTile(x, y, Tiles.CHALLENGETILE.getS());
		cases.add((new CaseChallenge(x, y, 12, CHALLENGE_TAB[5])).clone());

	}
	
	
	
	
	
	
	/**
	 * M�thode qui dessine un pion sur le canvas des joueurs
	 * @param x position sur l'axe X
	 * @param y position sur l'axe Y
	 * @param offsetX d�calage en X
	 * @param offsetY d�calage en Y
	 */
	public void drawPawn(Image img, double x, double y, double offsetX, double offsetY)
	{
		
		playerContext.save();
		playerContext.translate(((x - y) * TILE_WIDTH / 2), ((x + y) * TILE_HEIGHT / 2) - TILE_HEIGHT/10);
		playerContext.drawImage(img, (TILE_WIDTH/2.25) + offsetX + TILE_WIDTH*2 + ((WINDOW_WIDTH - (TILE_WIDTH*9)) / 2), (-IMG_HEIGHT/2) + offsetY + ((WINDOW_HEIGHT - (TILE_HEIGHT*5.5)) / 2), IMG_WIDTH, IMG_HEIGHT);
		
		playerContext.restore();
	}
	/**
	 * renvoie la case qui corespond a cette indice
	 * @param indOfCase numero de la case recherch�
	 * @return la case qui contient l'indice recherch�
	 */
	
	public Case rechercherCase(int indOfCase)
	{
		for (Case c : cases) {
			if(c.getInd() == indOfCase) return c;
		}
		
		return null;
	}
	/**
	 * nettoie le canvas des joueurs et redessine tout les pions au bon emplacement
	 */
	
	public void drawPlayerPawn()
	{
		//nettoie le canvas
		playerContext.translate(-350, -50);
		playerContext.clearRect(-200, -200, playerCanvas.getWidth()*2, playerCanvas.getHeight()*2);
		playerContext.translate(350, 50);
		//pour chaque joueur, on recupere les coordon�es de la case sur laquelle il est et on dessine le pion au m�me emplacement 

		for(int i = 0; i< players.size(); i++)
		{
			double x = rechercherCase(players.get(i).getIndCase()).getX();
			double y = rechercherCase(players.get(i).getIndCase()).getY();
			//String lien = Pawns.values()[i].getS();
			Image img = GenerationPionAvatar.getImagePawn().get(i);
			drawPawn(img, x, y, PAWN_OFFSET[0][i], PAWN_OFFSET[1][i]);
		}
	}
	
	public void addPlayer(Player p)
	{
		if(!players.contains(p))
		{
			players.add(p);
		}
	}
	public List<Player> getPlayersList()
	{
		List<Player> tmp = new ArrayList<>();
		for (Player player : players) {
			tmp.add(player);
		}
		return tmp;
	}
}

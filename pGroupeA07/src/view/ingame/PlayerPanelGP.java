package view.ingame;

import java.util.HashMap;
import java.util.Map;

import enumeration.Category;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;
import model.Player;

public class PlayerPanelGP extends GridPane{
	private Player p;
	private Label lblName;
	private Label lblScore;
	private ImageView avatar;
	private HashMap<Category, Rectangle> categories;
	
	private final int nbCol = 6;
	private final int hgap = 8;
	private final int vgap = 5;

	private static int nbPlayerPanel = 0;
	
	public PlayerPanelGP(String name) {
		nbPlayerPanel++;
		this.getStyleClass().add("playerPanel");
		this.setHgap(hgap);
		this.setVgap(vgap);
		this.categories = new HashMap<>();
		this.p = new Player(name);
		lblName = new Label(p.getName());
		lblScore = new Label(String.valueOf(p.getScore()));
		
		for(int i = 0; i<nbCol; i++)
		{
			ColumnConstraints cc = new ColumnConstraints();
			cc.setPercentWidth((float)100/nbCol);
			
			this.getColumnConstraints().add(cc);
		}
		Image image = GenerationPionAvatar.getImageAvatar().get(nbPlayerPanel-1);
		this.avatar = new ImageView(image);
		this.add(avatar, 0, 0,2,2);
		this.add(lblName, 2, 0, nbCol-2, 1);
		this.add(lblScore, 2, 1, nbCol-2, 1);
		lblName.setTextAlignment(TextAlignment.CENTER);
		lblScore.setTextAlignment(TextAlignment.CENTER);
		if(nbPlayerPanel %2 != 0) {
			creerPanelGauche();
		} else {
			creerPanelDroit();
		}
	}
	
	private void creerPanelGauche() {
		for(int i = 0; i< Category.values().length; i++) {
			Color color = Color.web(Category.values()[i].getColor());
			
			Rectangle rect = new Rectangle(15, 15, color);
			
			Tooltip t = new Tooltip(Category.values()[i].name());
			t.getStyleClass().add("tooltip");
			Tooltip.install(rect, t);
			rect.setOpacity(0.3);
			this.add(rect, i, 4);
			categories.put(Category.values()[i], rect);
			
		}
		this.getStyleClass().add("leftPlayerPanel");
	}
	
	private void creerPanelDroit() {
		for(int i = 0; i< Category.values().length; i++) {
			Color color = Color.web(Category.values()[i].getColor());
			
			Rectangle rect = new Rectangle(15, 15, color);
			
			Tooltip t = new Tooltip(Category.values()[i].name());
			Tooltip.install(rect, t);
			
			rect.setOpacity(0.3);
			
			this.add(rect, (nbCol - i)-1, 4);
			categories.put(Category.values()[i], rect);
		}
		this.getStyleClass().add("rightPlayerPanel");
	}
	
	public void validateCat(Category c)
	{
		categories.get(c).setOpacity(1);
		p.validateCat(c);
	}
	
	public void removeCat(Category c)
	{
		categories.get(c).setOpacity(0.1);
		p.RemoveCat(c);
	}
	
	public void updatePlayerScore()
	{
		p.increaseScore();
		lblScore.setText(String.valueOf(p.getScore()));
	}
	
	public boolean haveJustWin()
	{
		for (Map.Entry<Category, Boolean> entry : p.getCategoriesValidation()) {
			if (!entry.getValue()) return false;
		}
		return true;
	}
	
	public Label getLblName() {
		if(lblName == null)
		{
			lblName = new Label(p.getName());
		}
		return lblName;
	}
	
	public Player getPlayer()
	{
		return p;
	}

	public static void resetNbPlayerPanel() {
		PlayerPanelGP.nbPlayerPanel = 0;
	}
	
}

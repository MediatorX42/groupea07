package view.ingame;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import enumeration.Category;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Screen;
import javafx.util.Duration;
import model.GameManager;
import model.Player;
import model.deck.Question;
import model.tiles.Case;
import model.tiles.CaseChallenge;
import model.tiles.CaseJeux;
import view.ApplyDropShadow;
import view.GameViewSP;

public class QuestionDisplaySP extends MainInGameViewSP{
	
	
	private SubScene sbsRoll;
	private Random r = new Random();

	
	
	public QuestionDisplaySP(GameViewSP gvSP) {
		super(gvSP);
		
		
		
	}
	
	public SubScene getSbsRoll() {
		if (sbsRoll == null)
		{
			sbsRoll = GenerationDe.createScene3D();
			StackPane.setAlignment(sbsRoll, Pos.BOTTOM_CENTER);
			sbsRoll.getStyleClass().add("btnRoll");
			//Le label de remplacement sert a contourner le problème de 3d sur Manjaro
			Label lblRollRemplacement = new Label("Roll");
			this.getChildren().add(lblRollRemplacement);
			StackPane.setAlignment(lblRollRemplacement, Pos.BOTTOM_CENTER);
			lblRollRemplacement.toBack();
			lblRollRemplacement.setPadding(new Insets(20));
			lblRollRemplacement.setTextFill(Color.WHITE);
			//fait tourner le dé quand on clique dessus 
			sbsRoll.setOnMouseClicked(ev->{
				lblRollRemplacement.setTextFill(Color.CORNFLOWERBLUE);
				int diceNumber = 1 + r.nextInt(6);
				GenerationDe.randomRollDice();
				sbsRoll.setDisable(true);
				
				Timeline tl = new Timeline(new KeyFrame(Duration.seconds(2), event ->{
					GenerationDe.preciseNumberDice(diceNumber);
				}));
				//quand le dé s'arrete, on appel la méthode qui déplace le joueurs
				tl.setOnFinished(event -> {
					Timeline tl2 = new Timeline(new KeyFrame(Duration.seconds(1), event2 -> {
						movePlayer(diceNumber);
					}));
					tl2.setCycleCount(1);
					tl2.play();
					lblRollRemplacement.setTextFill(Color.WHITE);
				});
				tl.setCycleCount(1);
				tl.play();
			});
			sbsRoll.setVisible(true);
		}
		return sbsRoll;
	}

	
	@Override
	public void moveWhenNotEndGame(Timeline moving, Player p) {
		// TODO Auto-generated method stub
		moving.setOnFinished(questEvent->{
			//deplace le joueurs et affiche la question/challenge
			Case c = getPlateauP().rechercherCase(p.getIndCase());
			Category cat = null;
			if(c instanceof CaseJeux) {
				cat = ((CaseJeux)c).getCat();
				changeQuestion(cat);
			} else {
				displayChallenge((CaseChallenge) c);
			}
			
			getSbsRoll().setDisable(true);
			getSbsRoll().setVisible(false);
			
			Timeline t1 = new Timeline(new KeyFrame(Duration.seconds(0.5), ev->{
				for (Node node : this.getChildren()) {
					node.setEffect(new GaussianBlur());
				}
				getPresentationQuestion().setEffect(null);
				getPresentationQuestion().setVisible(true);
			}));
		
			t1.setCycleCount(1);
			t1.play();
		});
	}
	
	

	
	
	/**
	 * Cette methode permet de regenerer l'affichage global, et de proposer une nouvelle question.
	 */
	public void changeQuestion(Category cat)
	{
		resetDecompte();
		// Creation du vbTopOfBp qui servira de conteneur pour les informations
		getPresentationQuestion().getStyleClass().add("displayQuestion");

		// Initialisation de la question
		setCurrentQuestion(getGm().randomQuestion(cat));
		
		
		// Ajout de la categorie de la question
		setLblCategory(new Label(getCurrentQuestion().getCategory().name()));
		
		getLblCategory().setStyle("-fx-text-fill:"+getCurrentQuestion().getCategory().getColor());
		getLblCategory().setTextAlignment(TextAlignment.CENTER);
		
		// Ajout de l'intitule de la question
		setLblQuestion(new Label(getCurrentQuestion().getInterrogation())); 
		
		getLblQuestion().setWrapText(true);
		TextFlow tmp = new TextFlow(getLblQuestion());
		getLblQuestion().setTextAlignment(TextAlignment.CENTER);
		getLblQuestion().setAlignment(Pos.TOP_CENTER);

		VBox labels = new VBox(getLblDecompte(), getLblCategory(), tmp);
		getLblQuestion().prefWidthProperty().bind(labels.widthProperty());
		//TextFlow tmp = new TextFlow(lblQuestion);
		labels.setMaxHeight(500);
		labels.setAlignment(Pos.TOP_CENTER);
		
		// Ajout de l'interrogation et de la categorie au VBox, et placement de celui-ci au top du BorderPane
		getPresentationQuestion().getChildren().clear();
		getPresentationQuestion().getChildren().addAll(labels);
		getPresentationQuestion().setAlignment(Pos.CENTER);
		getPresentationQuestion().setSpacing(100);
		
		// On passe sur chaque choix de la question obtenue aleatoirement, et on ajoute un boutton correspondant a ce choix
		// On cree un VBox qui sera place au centre du BorderPane
		Set<Map.Entry<String, Boolean> > choices = getCurrentQuestion().getChoices();
		clearVbAnswers();
		getvbAnswers().setSpacing(10);
		
		
		for (Map.Entry<String, Boolean> entry : choices) {
		
			// Creation de chaque bouton contenant les choix de reponses a la question choisie
			Button btnAnswer = new Button(entry.getKey());
			btnAnswer.getStyleClass().add("btnAnswer");
			btnAnswer.setWrapText(true);
			getvbAnswers().getChildren().add(btnAnswer);
			
			// Pour chaque bouton, on ajoute un evenement qui se declenche lorsqu'une reponse est choisie par l'utilisateur
			btnAnswer.setOnAction(new EventHandler<ActionEvent>() {
			
				@Override
				public void handle(ActionEvent event) {
					
					// On desactive tous les boutons pour eviter que plusieurs choix soient faits
					for (Node n : getvbAnswers().getChildren()) {
						((Button)n).setStyle(null);
						n.setDisable(true);
					}
					
					//on arrete le chrono
					getAnimationDecompte().stop();
					
					// Si le choix est correct
					if(entry.getValue())
					{
						// Le choix est mis en vert pour signifier la bonne reponse au joueur
						btnAnswer.setStyle("-fx-background-color: lightgreen");
						// La categorie est validee pour le joueur correspondant
						getPlayers().get(getIndCurrentPlayer()).validateCat(getCurrentQuestion().getCategory());
						getPlayers().get(getIndCurrentPlayer()).updatePlayerScore();
					}
					else
					{
						// Le choix est mis en rouge pour signifier la mauvaise reponse au joueur
						btnAnswer.setStyle("-fx-background-color: lightcoral");
					}
					
					// Objet Timeline permettant de faire une temporisation avant la prochaine question. 
					Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1), ev->{					
						nextTurn();
					}));
					// Temporisation d'1 seconde
					tl.setCycleCount(1);
					tl.play();
					
				}
			});
		}
		// Placement du VBox contenant les questions
		getPresentationQuestion().getChildren().add(getvbAnswers());
		getPresentationQuestion().toFront();
		getvbAnswers().setAlignment(Pos.BOTTOM_CENTER);
	}
	
	
	public void displayChallenge(CaseChallenge c)
	{
		//le clear la vbox de présentation
		getPresentationQuestion().getStyleClass().add("displayQuestion");
		getPresentationQuestion().getChildren().clear();
		setLblQuestion(new Label(c.getChallenge()));
		setLblCategory(new Label("CHALLENGE"));
		
		getLblQuestion().setAlignment(Pos.CENTER);
		getLblCategory().setAlignment(Pos.CENTER);
		
		VBox labels = new VBox(getLblCategory(), getLblQuestion());
		labels.setAlignment(Pos.TOP_CENTER);
		
		Button b = new Button("OK");
		b.getStyleClass().add("btnAnswer");
		b.setOnAction(ev->{
			if(c.getChallenge().equals("Random Question !")){
				//je genere une categorie au hasard 
				int randNum = r.nextInt(6);
				//je pose une question de cette categorie 
				changeQuestion(Category.values()[randNum]);
			} else {
				nextTurn();
			}
		});
		getPresentationQuestion().getChildren().addAll(labels, b);
		getPresentationQuestion().setAlignment(Pos.CENTER);
		switch(c.getChallenge())
		{
			case "Skip your turn !" : 
				//je met le boolean frozen du joueur a true 
				getPlayers().get(getIndCurrentPlayer()).getPlayer().setFrozen(true);
				break;
				
			case "You just got teleported !":
				//je teste sur laquelle des 2 case le joueur est
				if(c.getInd() == 8)
				{
					//je change son indice dans la liste des QuesitonDisplay ainsi que dans la liste du plateau
					getPlayers().get(getIndCurrentPlayer()).getPlayer().setIndCase(24);
					getPlateauP().getPlayersList().get(getIndCurrentPlayer()).setIndCase(24);
					//je redessine les pions pour actualiser l'affichage 
					getPlateauP().drawPlayerPawn();
				}
				else
				{
					getPlayers().get(getIndCurrentPlayer()).getPlayer().setIndCase(8);
					getPlateauP().getPlayersList().get(getIndCurrentPlayer()).setIndCase(8);
					getPlateauP().drawPlayerPawn();
				}
				break;
				
			case "Random Question !":
				break;
				
			case "Everyone lose a category !":
				//je genere une categorie au hasard
				int randCat = r.nextInt(6);
				
				//j'affiche la categorie qui sera enlever a tout les joueurs 
				Label cat = new Label(Category.values()[randCat].name());
				cat.setTextFill(Paint.valueOf(Category.values()[randCat].getColor()));
				cat.setTextAlignment(TextAlignment.CENTER);
				
				getPresentationQuestion().getChildren().add(cat);
				//je parcours chaque joueurs et leurs enleve la categorie en question s'ils l'ont 
				for (int i = 0; i< getPlayers().size(); i++)
				{
					getPlayers().get(i).removeCat(Category.values()[randCat]);
					getPlateauP().getPlayersList().get(i).RemoveCat(Category.values()[randCat]);
				}
				break;
				
			case "You just received a free Category !":
				//je parcourt la liste des categorie du joueurs en cours
				for (Map.Entry<Category, Boolean> entry : getPlayers().get(getIndCurrentPlayer()).getPlayer().getCategoriesValidation()) 
				{
					//s'il ne possede pas la categorie, je lui attribue
					if(!entry.getValue()) {
						getPlayers().get(getIndCurrentPlayer()).validateCat(entry.getKey());
						getPlateauP().getPlayersList().get(getIndCurrentPlayer()).validateCat(entry.getKey());
						//j'affiche la categorie validée
						cat = new Label(entry.getKey().name());
						cat.setTextFill(Paint.valueOf(entry.getKey().getColor()));
						cat.setTextAlignment(TextAlignment.CENTER);
						
						getPresentationQuestion().getChildren().add(cat);
					
						break;
					}
				}
			break;
		}
	}
		
	
	

	
	/**
	 * Cette methode appelle les differentes methodes permettant de continuer la partie
	 */
	public void nextTurn()
	{
		nextPlayer();
		Label lb = new Label(getPlayers().get(getIndCurrentPlayer()).getLblName().getText() + ", it's your turn !");
		HBox hb = new HBox(lb);
		hb.setVisible(true);
		hb.getStyleClass().add("displayPlayerTurn");
		this.getChildren().add(hb);
		hb.setAlignment(Pos.CENTER);
		lb.setTextAlignment(TextAlignment.CENTER);
		lb.setAlignment(getAlignment());
		Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1.3), ev -> {
			hb.setVisible(false);
		}));
		tl.setCycleCount(1);
		tl.play();
		for (Node node : this.getChildren()) {
			node.setEffect(null);
		}
		getPresentationQuestion().setVisible(false);
		activePlayer();
		getSbsRoll().setDisable(false);
		getSbsRoll().setVisible(true);
	}
	
	
	
}

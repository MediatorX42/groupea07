package view.ingame;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import model.Player;
import view.ApplyDropShadow;
import view.GameViewSP;

public class VictoryBP extends BorderPane {
	private GameViewSP gvSP;
	private Label lblVict;
	private Label lblHighScore;
	
	public VictoryBP(GameViewSP gvSP)
	{
		this.gvSP = gvSP;
		
		this.lblVict = new Label();
		this.lblHighScore = new Label();
		this.lblVict.getStyleClass().add("whiteLabel");
		this.lblHighScore.getStyleClass().add("whiteLabel");
		
		VBox center = new VBox();
		center.getChildren().addAll(lblVict, lblHighScore);
		this.setCenter(center);
		
		BorderPane.setAlignment(center, Pos.CENTER);
		center.setAlignment(Pos.CENTER);
		lblVict.setTextAlignment(TextAlignment.CENTER);
		lblHighScore.setTextAlignment(TextAlignment.CENTER);
	
		Button btnQuit = new Button("Quit");
		btnQuit.setOnAction(ev -> {
			gvSP.resetGame();
		});
		btnQuit.setAlignment(Pos.BASELINE_CENTER);
		btnQuit.getStyleClass().add("btnQuitGameDisplay");
		this.setBottom(btnQuit);
		
		ApplyDropShadow.applyDropShadow(this.getChildren());
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void setPlayer(Player winningPlayer, List<Player> players)
	{
		this.lblVict.setText("Congratulations " + winningPlayer.getName() + ", you just won !");
		Collections.sort(players, new Comparator() {

			@Override
			public int compare(Object o1, Object o2) {
				return ((Player)o2).getScore() - ((Player)o1).getScore();
			}
		});
		String tmp = "";
		for (Player player : players) {
			tmp += player.getName() + " : " + player.getScore() + "\n"; 
		}
		this.lblHighScore.setText(tmp);
	}
}

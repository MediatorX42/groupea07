package view.menus;

import connexion.ConnexionControl;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import view.ApplyDropShadow;
import view.GameViewSP;

public class ConnexionBP extends BorderPane{
	private GameViewSP gvSP;
	private Label lblTitle;
	private VBox vbLogin;
	private HBox hbBtnLoginMenu;
	private Button btnConnexion;
	private Button btnQuit;
	private Button btnInscription;
	private Button btnGuest;
	
	private Label lblUsrName;
	private Label lblPassword;
	private TextField txtUsrName;
	private PasswordField pwdF;
	
	private Label lblWrongInfo;
	
	public ConnexionBP(GameViewSP gvsp)
	{
		this.gvSP = gvsp;
		// Positionnement du nom de l'application en haut de la fenêtre
		this.setTop(getLblTitle());
		BorderPane.setAlignment(getLblTitle(), Pos.CENTER);
		this.setCenter(getVbLogin());
		this.setBottom(getHbBtnLoginMenu());
		
		// Applique un effet d'ombre
		ApplyDropShadow.applyDropShadow(this.getChildren());
		
		// Affection d'un id pour recuperation dans le css
		this.setId("connexionBP");
	}
	
	public HBox getHbBtnLoginMenu() {
		if(hbBtnLoginMenu == null) {
			hbBtnLoginMenu = new HBox();
			hbBtnLoginMenu.getChildren().addAll(getBtnConnexion(), getBtnQuit());
			hbBtnLoginMenu.setAlignment(Pos.CENTER);
		}
		return hbBtnLoginMenu;
	}

	public VBox getVbLogin() {
		if(vbLogin == null) {
			vbLogin = new VBox();
			vbLogin.getChildren().addAll(getLblUsrName(), getTxtUsrName(), getLblPassword(), getPwdF(), getLblWrongInfo(), getBtnInscription(), getBtnGuest());
			vbLogin.setAlignment(Pos.CENTER);
		}
		return vbLogin;
	}
	
	public Label getLblTitle() {
		if(lblTitle == null) {
			lblTitle = new Label("GALILEO");
			lblTitle.getStyleClass().add("lblTitle");
			lblTitle.setTextAlignment(TextAlignment.CENTER);
		}
		return lblTitle;
	}
	
	public Label getLblUsrName() {
		if(lblUsrName == null) {
			lblUsrName = new Label("UserName");
		}
		return lblUsrName;
	}
	
	public TextField getTxtUsrName() {
		if(txtUsrName == null) {
			txtUsrName = new TextField();	
			txtUsrName.setId("test");
		}
		return txtUsrName;
	}
	
	public Label getLblPassword() {
		if(lblPassword == null) {
			lblPassword = new Label("Password");
		}
		return lblPassword;
	}

	public PasswordField getPwdF() {
		if(pwdF == null) {
			pwdF = new PasswordField();
		}
		return pwdF;
	}
	
	public Button getBtnConnexion() {
		if(btnConnexion == null) {
			btnConnexion = new Button("Connexion");
			btnConnexion.setOnAction(ev->{
				if(ConnexionControl.control(getTxtUsrName().getText(), getPwdF().getText())) {
					gvSP.setConnected(true);
					if(getTxtUsrName().getText().equals("admin")) 
						gvSP.setAdmin(true);
					gvSP.setUsrName(getTxtUsrName().getText());
					initGvSP();
					gvSP.rendreInvisibleSauf(gvSP.getMmBP());
					
				}
				else {
					getLblWrongInfo().setTextFill(Color.RED);
				}
			});
		}
		return btnConnexion;
	}
	
	public Button getBtnInscription() {
		if(btnInscription == null) {
			btnInscription = new Button("Still not registered ? click here !");
			btnInscription.setOnAction(ev->{
				gvSP.rendreInvisibleSauf(gvSP.getInscBP());
			});
		}
		return btnInscription;
	}
	
	
	public Button getBtnQuit() {
		if(btnQuit == null) {
			btnQuit = new Button("Quit");		
			btnQuit.setOnAction(ev -> Platform.exit());
			btnQuit.getStyleClass().add("btnMenuPrinc");
		}
		return btnQuit;
	}
	
	
	public Label getLblWrongInfo() {
		if(lblWrongInfo == null) {
			lblWrongInfo = new Label("UserName or password incorect !");
			lblWrongInfo.getStyleClass().add("noDoubAlert");
		}
		return lblWrongInfo;
	}
	
	public Button getBtnGuest() {
		if(btnGuest == null) {
			btnGuest = new Button("Or here to play as GUEST !");
			btnGuest.setOnAction(ev -> {
				initGvSP();
				gvSP.rendreInvisibleSauf(gvSP.getMmBP());
			});
			
		}

		return btnGuest;
	}
	
	public void initGvSP()
	{
		gvSP.getChildren().add(gvSP.getMmBP());
	}
}

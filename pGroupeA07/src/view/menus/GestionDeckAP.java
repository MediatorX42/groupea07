package view.menus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import Persistance.Persistance;
import enumeration.Category;
import exception.AlreadyPresentException;
import exception.CatAlreadyPresentException;
import exception.OnlyOneTrueException;
import exception.SizeLimitException;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import model.deck.Card;
import model.deck.Deck;
import model.deck.Question;
import view.GameViewSP;

public class GestionDeckAP extends StackPane {
	
	private GameViewSP gvSP;
	private AnchorPane anchorP;

	private TableView<String> tbVDecks;
	private TableView<Question> tbVQuestions;
	
	private Button btnAdd;
	private Button btnDelete;
	private Button btnReturn;
	
	private HBox hbBtn;
	private VBox vbQuestionModif;
	private Label lblQuestionInterogation;
	private List<TextField> txtAnswers;
	private int nbAnswers = 3;
	private final double PADDING = 10.;
	
	private VBox vbAddQuestion;
	private VBox vbRemoveQuest;
	
	private List<String> decksName;
	public GestionDeckAP(GameViewSP gvSP) {
		this.gvSP = gvSP;
		txtAnswers = new ArrayList<>();
		for(int i = 0; i< nbAnswers; i++)
		{
			txtAnswers.add(new TextField());
		}
		
		decksName = gvSP.getGameManager().getDecksName();
		
		this.getChildren().addAll(getAnchorP(), getVbAddQuestion(), getVbRemoveQuest());
		this.setAlignment(Pos.CENTER);
		this.setId("gestionDeck");
	}
	
	public AnchorPane getAnchorP() {
		if(anchorP == null)
		{
			anchorP = new AnchorPane(getTbVDecks(), getVbQuestionModif(), getTbVQuestions(), getHbBtn());
			
			AnchorPane.setTopAnchor(getTbVDecks(), PADDING);
			AnchorPane.setBottomAnchor(getTbVDecks(), (gvSP.getStageHeight() + PADDING*10) /2);
			AnchorPane.setLeftAnchor(getTbVDecks(), PADDING);
			AnchorPane.setRightAnchor(getTbVDecks(), PADDING + gvSP.getStageWidth()/2);
			
			AnchorPane.setTopAnchor(getTbVQuestions(), PADDING);
			AnchorPane.setBottomAnchor(getTbVQuestions(), PADDING*10);
			AnchorPane.setRightAnchor(getTbVQuestions(), PADDING);
			AnchorPane.setLeftAnchor(getTbVQuestions(), PADDING + gvSP.getStageWidth()/2);
			
			AnchorPane.setBottomAnchor(getHbBtn(), PADDING);

			AnchorPane.setBottomAnchor(getVbQuestionModif(), PADDING*10);
			AnchorPane.setLeftAnchor(getVbQuestionModif(), PADDING);
			AnchorPane.setRightAnchor(getVbQuestionModif(), PADDING + gvSP.getStageWidth()/2);
			AnchorPane.setTopAnchor(getVbQuestionModif(), (gvSP.getStageHeight() - PADDING*5) /2);
			
		}
		return anchorP;
	}

	public TableView<String> getTbVDecks() {
		if(tbVDecks == null) {
			tbVDecks = new TableView<String>();
			tbVDecks.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
			
			ObservableList<String> listDecks = FXCollections.observableArrayList(decksName);
					
			tbVDecks.setItems(listDecks);
			
			TableColumn<String, String> colNom = new TableColumn<>("Deck");
			
			colNom.setCellFactory(TextFieldTableCell.forTableColumn());
			colNom.setCellValueFactory(feature -> {
			    final String value = feature.getValue();
			    return new SimpleStringProperty(value);
			});
			
			tbVDecks.getColumns().add(colNom);
			
			//event pour update le tableaux des questions en fonction du decks
			tbVDecks.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) ->{
				if(newSelection != null)
				{
					Deck selectedDeck = getDeckByName(newSelection);
					getTbVQuestions().getItems().clear();
					for (Card c : selectedDeck.getCards()) {
						for (Question q : c.getQuestions()) {
							getTbVQuestions().getItems().add(q);
						}
					}	
				}
			});
		}
		return tbVDecks;
	}

	public TableView<Question> getTbVQuestions() {
		if(tbVQuestions == null) {
			tbVQuestions = new TableView<Question>();
			
			tbVQuestions.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
			tbVQuestions.setEditable(true);
			
			TableColumn<Question, String> colAuthor = new TableColumn<>("Author");
			TableColumn<Question, Category> colCategory = new TableColumn<>("Category");
			TableColumn<Question, String> colInterrogation = new TableColumn<>("Interrogation");
			
			colAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
			colCategory.setCellValueFactory(new PropertyValueFactory<>("category"));
			colInterrogation.setCellValueFactory(new PropertyValueFactory<>("interrogation"));
			
			colAuthor.setCellFactory(TextFieldTableCell.forTableColumn());
			colCategory.setCellFactory(ChoiceBoxTableCell.forTableColumn(Category.values()));
			colInterrogation.setCellFactory(TextFieldTableCell.forTableColumn());
			
			colAuthor.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Question, String>>(){

				@Override
				public void handle(CellEditEvent<Question, String> event) {
					
					Question oldQuest = event.getRowValue();
					Question newQuestion = new Question(event.getNewValue(), oldQuest.getCategory(), oldQuest.getInterrogation());
					File f = new File("res/deck");
					File listf[] = f.listFiles();
					for (File file : listf) {
						
						if ((file.getName().substring(0, file.getName().length() - 5).equals(getTbVDecks().getSelectionModel().getSelectedItem())))
						{
							try {
								Deck d = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
								
								for (Card c : d.getCards()) {
									if(c.getQuestions().contains(oldQuest))
									{
										
										
										if(c.updateAuthor(oldQuest, newQuestion)) {
											event.getRowValue().setAuthor(event.getNewValue()); 
											Persistance.ecrireJson(file.getCanonicalPath(), d);
										}
									}
								}
							} catch (IOException e) {
								continue;
							}
							
						}
					}
				}
				
			});
			
			colCategory.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Question, Category>>(){

				@Override
				public void handle(CellEditEvent<Question, Category> event) {
					
					Question oldQuest = event.getRowValue();
					Question newQuestion = new Question(oldQuest.getAuthor(), event.getNewValue(), oldQuest.getInterrogation());
					File f = new File("res/deck");
					File listf[] = f.listFiles();
					for (File file : listf) {
						
						if ((file.getName().substring(0, file.getName().length() - 5).equals(getTbVDecks().getSelectionModel().getSelectedItem())))
						{
							try {
								Deck d = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
								
								for (Card c : d.getCards()) {
									if(c.getQuestions().contains(oldQuest))
									{
										System.out.println("Test");
										
										if(c.updateCatAndIntero(oldQuest, newQuestion)) {
											event.getRowValue().setCategory(event.getNewValue());
											System.out.println(d);
											Persistance.ecrireJson(file.getCanonicalPath(), d);
										}
									}
								}
							} catch (IOException e) {
								continue;
							}
							
						}
					}
				}
				
			});
			
			colInterrogation.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Question, String>>(){

				@Override
				public void handle(CellEditEvent<Question, String> event) {
					
					Question oldQuest = event.getRowValue();
					Question newQuestion = new Question(oldQuest.getAuthor(), oldQuest.getCategory(), event.getNewValue());
					File f = new File("res/deck");
					File listf[] = f.listFiles();
					for (File file : listf) {
						
						if ((file.getName().substring(0, file.getName().length() - 5).equals(getTbVDecks().getSelectionModel().getSelectedItem())))
						{
							try {
								Deck d = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
								
								for (Card c : d.getCards()) {
									if(c.getQuestions().contains(oldQuest))
									{
										System.out.println("Test");
										
										if(c.updateCatAndIntero(oldQuest, newQuestion)) {
											event.getRowValue().setInterrogation(event.getNewValue());
											getLblQuestionInterogation().setText(event.getNewValue());
											
											Persistance.ecrireJson(file.getCanonicalPath(), d);
										}
									}
								}
							} catch (IOException e) {
								continue;
							}
							
						}
					}
				}
				
			});	
			tbVQuestions.getColumns().addAll(colAuthor, colCategory, colInterrogation);
			
			tbVQuestions.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) ->{
				if(newSelection != null)
				{
					getLblQuestionInterogation().setText(newSelection.getInterrogation());
					int i = 0;
					for (Map.Entry<String, Boolean> entry : newSelection.getChoices()) {
						txtAnswers.get(i).setText(entry.getKey());
						if(entry.getValue())
						{
							txtAnswers.get(i).setStyle("-fx-background-color: green");
						}
						else
						{
							txtAnswers.get(i).setStyle("-fx-background-color: red");
						}
						
						
						i++;
					}
					
				}
			});
			
		}
		return tbVQuestions;
	}

	public Button getBtnAdd() {
		if(btnAdd == null) {
			btnAdd = new Button("Add");
			btnAdd.getStyleClass().add("btnQuitGameDisplay");
			
			btnAdd.setOnAction(ev->{
				getVbAddQuestion().setVisible(!getVbAddQuestion().isVisible());
			});
		}
		return btnAdd;
	}

	public Button getBtnDelete() {
		if(btnDelete == null) {
			btnDelete = new Button("Delete");
			btnDelete.getStyleClass().add("btnQuitGameDisplay");
			btnDelete.setOnAction(ev->{
				if(getTbVQuestions().getSelectionModel().getSelectedItem() != null)
				{
					getVbRemoveQuest().setVisible(true);
				}
				
			});
		}
		return btnDelete;
	}
	
	public Button getBtnReturn() {
		if(btnReturn == null)
		{
			btnReturn = new Button("Return");
			btnReturn.getStyleClass().add("btnQuitGameDisplay");
			
			btnReturn.setOnAction(ev->{
				gvSP.rendreInvisibleSauf(gvSP.getMmBP());
				gvSP.getChildren().remove(gvSP.getGdAP());
				gvSP.resetAllNode();
			});
		}
		return btnReturn;
	}
	
	public HBox getHbBtn() {
		if(hbBtn == null) {
			if(gvSP.isAdmin())
				hbBtn = new HBox(getBtnAdd(), getBtnDelete(), getBtnReturn());
			else
				hbBtn = new HBox(getBtnReturn());
			hbBtn.setAlignment(Pos.BOTTOM_CENTER);
		}
		return hbBtn;
	}
	
	public VBox getVbQuestionModif() {
		if(vbQuestionModif == null)
		{
			vbQuestionModif = new VBox();
			vbQuestionModif.setStyle("-fx-background-color: white");
			vbQuestionModif.setId("vbQuestionModif");
			Label interro = new Label("Interogation :");
			interro.setWrapText(true);
			
			Label ans = new Label("Answers :");
			Button btnSave = new Button("Save answers");
			btnSave.getStyleClass().add("btnAnswer");
			btnSave.setOnAction(ev->{
				HashMap<String, Boolean> newChoices = new HashMap<>();
				for(int i = 0; i< nbAnswers; i++)
				{
					Boolean isCorrect = false;
					if(txtAnswers.get(i).getStyle().equals("-fx-background-color: green")) {
						isCorrect = true;
					}
					newChoices.put(txtAnswers.get(i).getText(), isCorrect);
				}
				
				File f = new File("res/deck");
				File listf[] = f.listFiles();
				Question quest = getTbVQuestions().getSelectionModel().getSelectedItem();
				for (File file : listf) {
					
					if ((file.getName().substring(0, file.getName().length() - 5).equals(getTbVDecks().getSelectionModel().getSelectedItem())))
					{
						try {
							Deck d = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
							
							for (Card c : d.getCards()) {
								if(c.getQuestions().contains(quest))
								{
									if(c.updateChoices(quest, newChoices))
									{
										System.out.println("Test");
										Persistance.ecrireJson(file.getCanonicalPath(), d);
									}
								}
							}
						} catch (IOException e) {
							continue;
						}
					}
				}
			});
			
			vbQuestionModif.getChildren().addAll(interro, getLblQuestionInterogation(), ans, txtAnswers.get(0), txtAnswers.get(1), txtAnswers.get(2));
			if(gvSP.isAdmin())
			{
				vbQuestionModif.getChildren().add(btnSave);
			}
			vbQuestionModif.setAlignment(Pos.CENTER);
		}
		return vbQuestionModif;
	}
	
	public Label getLblQuestionInterogation() {
		if(lblQuestionInterogation == null)
		{
			lblQuestionInterogation = new Label();
			lblQuestionInterogation.setStyle("-fx-font-size: 20");
		}
		return lblQuestionInterogation;
	}
	
	public VBox getVbAddQuestion() {
		if(vbAddQuestion == null)
		{
			vbAddQuestion = new VBox();
			vbAddQuestion.setStyle("-fx-background-color: white");
			Label intero = new Label("Interogation :");
			TextField txtintero = new TextField();
			Label lblCat = new Label("Category :");
			ComboBox<Category> cbbCat = new ComboBox<>();
			cbbCat.setItems(FXCollections.observableList(Arrays.asList(Category.values())));
			Label answers = new Label("Answers :");
			vbAddQuestion.getChildren().addAll(intero, txtintero,lblCat, cbbCat , answers);
			List<TextField> listTxtAnswers = new ArrayList<>();
			for(int i = 0; i< nbAnswers; i++)
			{
				TextField tmp = new TextField();
				listTxtAnswers.add(tmp);
				vbAddQuestion.getChildren().add(tmp);
				if(i == 0)
				{
					listTxtAnswers.get(i).setStyle("-fx-background-color: green");
				}
				else
				{
					listTxtAnswers.get(i).setStyle("-fx-background-color: red");
				}
			}
			listTxtAnswers.get(0).setStyle("-fx-background-color: green");
			Label lblError = new Label();
			lblError.getStyleClass().add("noDoubAlert");
			Button btn = new Button("Add Question");
			btn.getStyleClass().add("btnAnswer");
			btn.setOnAction(ev->{
				boolean choicesProblem = false;
				if(!txtintero.getText().equals("") && cbbCat.getValue() != null)
				{
					Question qToAdd = new Question("admin", cbbCat.getValue(), txtintero.getText());
					for(int i = 0; i< listTxtAnswers.size(); i++)
					{
						
						try {
							if(i==0)
							{
								qToAdd.addChoices(listTxtAnswers.get(i).getText(), true);
							}
							else
							{
								qToAdd.addChoices(listTxtAnswers.get(i).getText(), false);
							}
							
						} catch (AlreadyPresentException | OnlyOneTrueException e) {
							
							choicesProblem = true;
							lblError.setText("Every choices must be diferent !");
							lblError.setTextFill(Color.RED);
						}
						
					}
					
					if(!choicesProblem)
					{
						Deck d = null;
						try {
							d = Persistance.lireJson("res/deck/adminDeck.json", Deck.class);
						} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e1) {
							
							e1.printStackTrace();
							
						}
						Boolean hasBeenAdd = false;
						for (Card c : d.getCards()) {
							try {
								c.addQuestion(qToAdd);
								hasBeenAdd = true;
								break;
							} catch (AlreadyPresentException e) {
								
								lblError.setText(e.getMessage());
								lblError.setTextFill(Color.RED);
							} catch (CatAlreadyPresentException | SizeLimitException e) {
								continue;
							} 
						}
						if(!hasBeenAdd)
						{
							Card c = new Card();
							try {
								c.addQuestion(qToAdd);
							} catch (AlreadyPresentException | CatAlreadyPresentException | SizeLimitException e) {
								
								lblError.setText("The question already exist !");
								lblError.setTextFill(Color.RED);
							}
							d.addCard(c);
						}
						Persistance.ecrireJson("res/deck/adminDeck.json", d);
						getVbAddQuestion().setVisible(false);
					}
					else
					{
						lblError.setText("All fields must be completed !");
						lblError.setTextFill(Color.RED);
					}
					
				}
				else
				{
					lblError.setText("All fields must be completed !");
					lblError.setTextFill(Color.RED);
				}
				
			
			});
			vbAddQuestion.getChildren().addAll(lblError, btn);
			vbAddQuestion.getStyleClass().add("displayQuestion");
			vbAddQuestion.setAlignment(Pos.CENTER);
			vbAddQuestion.setVisible(false);
			
		}
		return vbAddQuestion;
	}
	
	public VBox getVbRemoveQuest() {
		if(vbRemoveQuest == null)
		{
			
			Label wantToRemove = new Label("Do you really want to remove this question ?");
			
			Button yes = new Button("Yes");
			yes.getStyleClass().add("btnAnswer");
			yes.setOnAction(ev->{
				Question qToDelete = getTbVQuestions().getSelectionModel().getSelectedItem();
				File f = new File("res/deck");
				File listf[] = f.listFiles();
				for (File file : listf) {
					
					if ((file.getName().substring(0, file.getName().length() - 5).equals(getTbVDecks().getSelectionModel().getSelectedItem())))
					{
						try {
							Deck d = Persistance.lireJson(file.getCanonicalPath(), Deck.class);
							
							for (Card c : d.getCards()) {
								if(c.getQuestions().contains(qToDelete))
								{
									System.out.println("Test");
									
									if(c.deleteQuestion(qToDelete)) {
										Persistance.ecrireJson(file.getCanonicalPath(), d);
										getVbRemoveQuest().setVisible(false);
									}
								}
							}
						} catch (IOException e) {
							continue;
						}
						
					}
				}
				
			});
			Button no = new Button("No");
			no.setOnAction(ev->{
				vbRemoveQuest.setVisible(false);
			});
			no.getStyleClass().add("btnAnswer");
			HBox yesOrNo = new HBox(yes, no);
			vbRemoveQuest = new VBox(wantToRemove, yesOrNo);
			vbRemoveQuest.getStyleClass().add("displayQuestion");
			vbRemoveQuest.setAlignment(Pos.CENTER);
			vbRemoveQuest.setVisible(false);
			
					
		}
		return vbRemoveQuest;
	}
	
	

	public List<Question> extractQuestions(){
		File f = new File("res/deck");
		File listf[] = f.listFiles();
		List<Deck> decks = new ArrayList<>();
		for (File file : listf) {
			try {
				decks.add(Persistance.lireJson(file.getCanonicalPath(), Deck.class));
			} catch (IOException e) {
				continue;
			}
		}
		List<Card> cards = new ArrayList<>();
		for (Deck deck : decks) {
			cards.addAll(deck.getCards());
		}
		List<Question> questions = new ArrayList<>();
		for (Card card : cards) {
			questions.addAll(card.getQuestions());
		}
		return questions;
	}

	public Deck getDeckByName(String name)
	{
		File f = new File("res/deck");
		File listf[] = f.listFiles();
		for (File file : listf) {
			String fileName = file.getName().substring(0, file.getName().length() - 5);
			if(fileName.equals(name))
			{
				try {
					return Persistance.lireJson(file.getCanonicalPath(), Deck.class);
				} catch (IOException e) {
					continue;
				}
			}
		}
		return null;
	}


	
}

package view.menus;

import connexion.ConnexionControl;
import exception.EmailIncorectException;
import exception.UserNameOrEmailTakenException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import view.ApplyDropShadow;
import view.GameViewSP;

public class InscriptionBP extends BorderPane{
	private GameViewSP gvSP;
	private Label lblTitle;
	private VBox vbLogin;
	private HBox hbBtnLoginMenu;
	private Button btnCancel;
	private Button btnInscription;
	
	private Label lblUsrName;
	private Label lblPassword;
	private Label lblEmail;
	private TextField txtUsrName;
	private PasswordField pwdF;
	private TextField txtEmail;
	
	private Label lblWrongInfo;
	
	public InscriptionBP(GameViewSP gvsp)
	{
		this.gvSP = gvsp;
		// Positionnement du nom de l'application en haut de la fenêtre
		this.setTop(getLblTitle());
		BorderPane.setAlignment(getLblTitle(), Pos.CENTER);
		this.setCenter(getVbLogin());
		this.setBottom(getHbBtnLoginMenu());
		
		// Applique un effet d'ombre
		ApplyDropShadow.applyDropShadow(this.getChildren());
		
		// Affection d'un id pour recuperation dans le css
		this.setId("inscriptionBP");
	}
	
	public HBox getHbBtnLoginMenu() {
		if(hbBtnLoginMenu == null) {
			hbBtnLoginMenu = new HBox();
			hbBtnLoginMenu.getChildren().addAll(getBtnInscription(), getBtnCancel());
			hbBtnLoginMenu.setAlignment(Pos.CENTER);
		}
		return hbBtnLoginMenu;
	}

	public VBox getVbLogin() {
		if(vbLogin == null) {
			vbLogin = new VBox();
			vbLogin.getChildren().addAll(getLblUsrName(), getTxtUsrName(), getLblEmail(), getTxtEmail(), getLblPassword(), getPwdF(), getLblWrongInfo(), getBtnInscription());
			vbLogin.setAlignment(Pos.CENTER);
		}
		return vbLogin;
	}
	
	public Label getLblTitle() {
		if(lblTitle == null) {
			lblTitle = new Label("GALILEO");
			lblTitle.getStyleClass().add("lblTitle");
			lblTitle.setTextAlignment(TextAlignment.CENTER);
		}
		return lblTitle;
	}
	
	public Label getLblUsrName() {
		if(lblUsrName == null) {
			lblUsrName = new Label("UserName");
		}
		return lblUsrName;
	}
	
	public TextField getTxtUsrName() {
		if(txtUsrName == null) {
			txtUsrName = new TextField();			
		}
		return txtUsrName;
	}
	
	public Label getLblPassword() {
		if(lblPassword == null) {
			lblPassword = new Label("Password");
		}
		return lblPassword;
	}
	
	public Label getLblEmail() {
		if(lblEmail == null)
		{
			lblEmail = new Label("Mail");
		}
		return lblEmail;
	}

	public PasswordField getPwdF() {
		if(pwdF == null) {
			pwdF = new PasswordField();
		}
		return pwdF;
	}
	
	public TextField getTxtEmail() {
		if(txtEmail == null)
		{
			txtEmail = new TextField();
		}
		return txtEmail;
	}
	
	
	
	public Button getBtnInscription() {
		if(btnInscription == null) {
			btnInscription = new Button("Register");
			btnInscription.getStyleClass().add("btnMenuPrinc");
			btnInscription.setOnAction(ev->{
				try {
					ConnexionControl.registerNewUser(getTxtUsrName().getText(), getPwdF().getText(), getTxtEmail().getText()); 
					
					gvSP.setConnected(true);
					gvSP.setUsrName(getTxtUsrName().getText());
					gvSP.getCoBP().initGvSP();
					gvSP.rendreInvisibleSauf(gvSP.getMmBP());
		
				} catch (UserNameOrEmailTakenException | EmailIncorectException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					getLblWrongInfo().setText(e.getMessage());
					getLblWrongInfo().setTextFill(Color.RED);
				}
			});
		}
		return btnInscription;
	}
	
	
	public Button getBtnCancel() {
		if(btnCancel == null) {
			btnCancel = new Button("Cancel");		
			btnCancel.setOnAction(ev -> gvSP.rendreInvisibleSauf(gvSP.getCoBP()));
			btnCancel.getStyleClass().add("btnMenuPrinc");
		}

		return btnCancel;
	}
	
	
	public Label getLblWrongInfo() {
		if(lblWrongInfo == null) {
			lblWrongInfo = new Label("Error still to do");
			lblWrongInfo.getStyleClass().add("noDoubAlert");
		}
		return lblWrongInfo;
	}
}

package view.menus;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import view.ApplyDropShadow;
import view.ColorPickerBP;
import view.GameViewSP;
import view.ingame.GenerationPionAvatar;

/**
 * Cette classe va permettre de creer une interface de jeu en local.
 * Elle demande le nombre de joueurs, et propose le nombre de labels et de textfields adapte.
 * Les labels et textfields vont servir a indiquer le nom de chaque joueur participant.
 * La strucutre de l'interface utilise des HBox et VBox pour repartir les elements de maniere 
 * verticale ou horizontale simplement.
 */
public class LocalGameBP extends BorderPane {
	
	private static final int NB_MAX_JOUEURS = 8;
	private GameViewSP gvSP;
	
	private VBox vbConteneur;
	
	private GridPane ListPlayerGP;
	private HBox hbNbPlayer;
	private HBox hbButtonsBottom;
	private Button btnPlay, btnReturn;
	private Label lblDoubName;
	
	private Label lblNbPlayer;
	private ComboBox<Integer> cbbNbPlayer;

	
	/**
	 * Le constructeur va instancier un objet GameView recu en argument.
	 * @param gvSP Classe global reprenant tous les layouts
	 */
	public LocalGameBP(GameViewSP gvSP) {
		this.gvSP = gvSP;
		this.setCenter(getVbConteneur());
		DropShadow ds = new DropShadow();
		ds.setOffsetY(10.0);
		ds.setColor(Color.web("#212121"));
		ApplyDropShadow.applyDropShadow(this.getChildren());
		this.setId("localGameBP");
	}

	/**
	 * Ce getter permet d'acceder a une Vertical Box, qui va contenir tous les elements de l'interface.
	 * Elle contient 3 autres "conteneurs" : HbNbPlayer, VbListPlayer et getHbButtonsBottom.
	 * Elle va egalement etre placee au centre de l'interface.
	 * L'interet de cette methode est de placer facilement de maniere verticale tous les elements a afficher.
	 * 
	 * @return vbConteneur Vertical Box contenant tous les autres conteneurs
	 */
	public VBox getVbConteneur() {
		if(vbConteneur == null) {
			vbConteneur = new VBox();
			vbConteneur.getChildren().addAll(getHbNbPlayer(), getListPlayerGP(), getLblDoubName(), getHbButtonsBottom());
			vbConteneur.setAlignment(Pos.CENTER);
			vbConteneur.setMaxWidth(200);
			vbConteneur.setSpacing(10);
		}
		return vbConteneur;
	}
	
	/**
	 * Ce getter permet d'acceder a la Vertical Box qui propose un TextField pour chaque joueur
	 * Il est complete par la methode genererChamps()
	 * 
	 * @return getVbListPlayer VerticalBox avec Label et TextField
	 */
	public GridPane getListPlayerGP() {
		if(ListPlayerGP == null) {
			ListPlayerGP = new GridPane();
			ListPlayerGP.setAlignment(Pos.CENTER);
		}
		return ListPlayerGP;
	}
	
	/**
	 * Ce getter permet d'acceder a la HorizontalBox qui contient un Label et une ComboBox,
	 * permettant d'indiquer le nombre de joueur qui participeront au jeu.
	 * 
	 * @return hbNbPlayer HBox pour le nombre de joueurs
	 */
	public HBox getHbNbPlayer() {
		if(hbNbPlayer == null) {
			hbNbPlayer = new HBox();
			hbNbPlayer.getChildren().addAll(getLblNbPlayer(), getCbbNbPlayer());
			hbNbPlayer.setAlignment(Pos.CENTER);
		}
		return hbNbPlayer;
	}

	/**
	 * Ce getter permet d'acceder au HBox contenant les boutons "Play" et "Quit"
	 * @return hbButtonsBottom HBox contenant les boutons Play et Quit
	 */
	public HBox getHbButtonsBottom() {
		if(hbButtonsBottom == null) {
			hbButtonsBottom = new HBox();
			hbButtonsBottom.setAlignment(Pos.CENTER);
			hbButtonsBottom.getChildren().addAll(getBtnPlay(), getBtnReturn());
		}
		return hbButtonsBottom;
	}
	/**
	 * Ce getter permet d'acceder au label du nombre de joueurs
	 * 
	 * @return lblNbPlayer Label demandant le nombre de joueurs
	 */
	public Label getLblNbPlayer() {
		if(lblNbPlayer == null) {
			lblNbPlayer = new Label("Number of players : ");
		}
		return lblNbPlayer;
	}

	/**
	 * Ce getter permet d'acceder au bouton play. Il va appeler la methode getQdBP(),
	 * methode de l'objet gvSP qui est dans le GameView. 
	 * Concretement, il permet d'acceder a l'interface de jeu
	 * 
	 * @return btnPlay Bouton play
	 */
	public Button getBtnPlay() {
		if(btnPlay == null) {
			btnPlay = new Button("Play");
			btnPlay.setOnAction(ev -> {
				
				boolean allPlayersDifferents = true;
				
				Set<Node> setTf = this.lookupAll("TextField");
				List<String> textFields = new ArrayList<>();
				
				for (Node node : setTf) {
					TextField tmp = (TextField)node;
					if(textFields.contains(tmp.getText()) || tmp.getText().isEmpty()) {
						allPlayersDifferents = false;
					}
					textFields.add(tmp.getText());
				}
				
				if(allPlayersDifferents)
				{
					gvSP.getChildren().add(gvSP.getQdBP());
					gvSP.rendreInvisibleSauf(gvSP.getQdBP());
					gvSP.getQdBP().initializePlayer(textFields);
					gvSP.getQdBP().displayPlayersInfo();
				} else {
					getLblDoubName().setTextFill(Color.RED);
				}
			});
		}
		return btnPlay;
	}

	/**
	 * Ce getter permet d'acceder au bouton return. Il va appeler la methode getMmBP(),
	 * methode de l'objet getMmBP qui est dans le GameView. 
	 * Concretement, il permet d'acceder a l'interface du menu principal
	 * 
	 * @return btnReturn Bouton return
	 */
	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Return");
			btnReturn.setOnAction(ev -> 
			{
				gvSP.rendreInvisibleSauf(gvSP.getMmBP());
				gvSP.getChildren().remove(gvSP.getLgBP());
				gvSP.resetAllNode();
			});
		}
		return btnReturn;
	}

	/**
	 * Ce getter permet d'acceder a la ComboBox qui demande le nombre de joueurs. 
	 * Il remplit la ComboBox de 1 jusqu'au nombre max de joueur indique dans la variable NB_MAX_JOUEURS
	 * Il definit egalement un evenement sur la ComboBox, qui va appeler la methode genererChamps(int)
	 * 
	 * @return cbbNbPlayer ComboBox permettant d'indiquer le nombre de joueurs
	 */
	public ComboBox<Integer> getCbbNbPlayer() {
		if(cbbNbPlayer == null) {
			cbbNbPlayer = new ComboBox<Integer>();
			for(int i = 1 ; i <= NB_MAX_JOUEURS ; i++) cbbNbPlayer.getItems().add(i);
			cbbNbPlayer.setValue(1);
			genererChamps(cbbNbPlayer.getValue());
			cbbNbPlayer.setOnAction(ev -> genererChamps(cbbNbPlayer.getValue()));
		}
		return cbbNbPlayer;
	}
	
	
	public Label getLblDoubName() {
		if(lblDoubName == null) {
			lblDoubName = new Label("Use distinct names !");
			lblDoubName.getStyleClass().add("noDoubAlert");
		}
		return lblDoubName;
	}

	/**
	 * La methode genererChamps permet de creer, par joueur  dans la ComboBox cbbNbPlayer
	 * @param nbPlayer
	 */
	public void genererChamps(int nbPlayer) {
		getListPlayerGP().getChildren().clear();
		ColorPickerBP.getColorPickerBPs().clear();
		for(int j = 0 ; j < nbPlayer ; j++) {
			TextField champsPourNom = new TextField();
			Pattern pattern = Pattern.compile(".{0,10}");
		    @SuppressWarnings({ "unchecked", "rawtypes" })
			TextFormatter<?> formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
		        return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		    });
		    champsPourNom.setTextFormatter(formatter);
		    
		    Label playerAndNum = new Label("Player "+(j+1));
		    Rectangle rect = new Rectangle();
		    rect.setWidth(30);
		    rect.setHeight(30);
		    rect.getStyleClass().add("rectColorPicker");
		    ColorPickerBP cp = new ColorPickerBP(j);
		    cp.setId(String.valueOf(j));
		    cp.fillARect(rect);
		    cp.setOnMouseClicked(ev -> {
		    	cp.fillARect(rect);
		    });
		    cp.setOnMouseReleased(ev -> {
		    	cp.fillARect(rect);
		    	GenerationPionAvatar.generatePawnAvatar(Color.valueOf(rect.getId()), cp.getId());
		    });
		    cp.setVisible(false);
		    rect.setOnMouseClicked(ev -> {
		    	if(cp.isVisible()) {
		    		cp.setVisible(false);
		    	} else {
		    		cp.setVisible(true);
		    	}
		    });
		    GridPane gp = new GridPane();
		    gp.setPrefWidth(800);
		    for(int i = 0; i<10; i++)
			{
				ColumnConstraints cc = new ColumnConstraints();
				cc.setPercentWidth((float)100/10);
				gp.getColumnConstraints().add(cc);
			}
		    gp.add(playerAndNum, 0, 0, 3, 1);
		    gp.add(champsPourNom, 3, 0, 3, 1);
		    gp.add(rect, 7, 0);
		    gp.add(cp, 8, 0,2,1);
			getListPlayerGP().add(gp, 1, j);
		}
		GenerationPionAvatar.fillList();
	}
}

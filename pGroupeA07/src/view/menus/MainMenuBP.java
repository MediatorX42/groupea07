package view.menus;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import view.ApplyDropShadow;
import view.GameViewSP;

public class MainMenuBP extends BorderPane {
	
	private GameViewSP gvSP;
	
	private Label lblTitle;
	
	private Button btnLocal;
	private Button btnOnline;
	private Button btnParameters;
	private Button btnQuit;
	private Button btnDeck;
	
	private VBox vbMenu;
	
	public MainMenuBP(GameViewSP gvSP) {
		this.gvSP = gvSP;
		
		// Positionnement du nom de l'application en haut de la fenêtre
		this.setTop(getLblTitle());
		BorderPane.setAlignment(getLblTitle(), Pos.CENTER);
		this.setCenter(getVbMenu());
		
		// Applique un effet d'ombre
		ApplyDropShadow.applyDropShadow(this.getChildren());
		
		// Affection d'un id pour recuperation dans le css
		this.setId("mainMenuBP");
	}
	
	public VBox getVbMenu() {
		if(vbMenu == null) {
			vbMenu = new VBox();
			if(gvSP.isConnected()){
				vbMenu.getChildren().addAll(getBtnLocal(), getBtnOnline(), getBtnDeck(), getBtnParameters(), getBtnQuit());
			}
			else{
				vbMenu.getChildren().addAll(getBtnLocal(), getBtnParameters(), getBtnQuit());
			}
			
			if(gvSP.isAdmin()) vbMenu.setSpacing(-20);
			vbMenu.setAlignment(Pos.CENTER);
		}
		return vbMenu;
	}

	public Button getBtnLocal() {
		if(btnLocal == null) {
			btnLocal = new Button("Play");
			btnLocal.setOnAction(ev -> {
				gvSP.getChildren().add(gvSP.getLgBP());
				gvSP.rendreInvisibleSauf(gvSP.getLgBP());
				
			});
			btnLocal.getStyleClass().add("btnMenuPrinc");
		}
		return btnLocal;
	}
	
	public Button getBtnOnline() {
		if(btnOnline == null) btnOnline = new Button("Online");
		btnOnline.setOnAction(ev -> {
			gvSP.getChildren().add(gvSP.getOnBP());
			gvSP.rendreInvisibleSauf(gvSP.getOnBP());
		});
		btnOnline.getStyleClass().add("btnMenuPrinc");
		return btnOnline;
	}
	
	public Button getBtnDeck() {
		if(btnDeck == null) {
			btnDeck = new Button("Deck");
			btnDeck.setOnAction(ev ->{
				gvSP.getChildren().add(gvSP.getGdAP());
				gvSP.rendreInvisibleSauf(gvSP.getGdAP());
			});
			btnDeck.getStyleClass().add("btnMenuPrinc");
			if(!gvSP.isAdmin()) btnDeck.setVisible(true);
		}
		return btnDeck;
	}

	public Button getBtnParameters() {
		if(btnParameters == null) {
			btnParameters = new Button("Parameters");
			btnParameters.setOnAction(ev -> {
				gvSP.rendreInvisibleSauf(gvSP.getPaBP());
				gvSP.getChildren().add(gvSP.getPaBP());
				
			});
			btnParameters.getStyleClass().add("btnMenuPrinc");
		}
		return btnParameters;
	}
	
	public Button getBtnQuit() {
		if(btnQuit == null) {
			btnQuit = new Button("Quit");		
			btnQuit.setOnAction(ev -> {
				Platform.exit();
				System.exit(0);
			});
			btnQuit.getStyleClass().add("btnMenuPrinc");
		}

		return btnQuit;
	}
	
	public Label getLblTitle() {
		if(lblTitle == null) {
			lblTitle = new Label("GALILEO");
			lblTitle.getStyleClass().add("lblTitle");
			lblTitle.setTextAlignment(TextAlignment.CENTER);
		}
		return lblTitle;
	}
}
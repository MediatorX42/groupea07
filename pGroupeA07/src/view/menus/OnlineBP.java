package view.menus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import enumeration.Category;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import message.Message;
import message.Operation;
import model.deck.Question;
import view.GameViewSP;
import view.ingame.GenerationPionAvatar;

// Cette classe permet d'afficher une interface de connexion a une partie en ligne
public class OnlineBP extends StackPane implements Serializable{

	private static final long serialVersionUID = 1L;
	private Socket socket;
	private ObjectOutputStream bufferedWriter;
	private ObjectInputStream bufferedReader;
	private String userName;
	private TextArea txtAreaLobby;
	private GameViewSP gvSP;
	private Button btnReady, btnReturn;
	private TextField txtMessage;
	private VBox vbConteneur;
	private List<String> players;
	private ScrollPane messagesSP;
	private FlowPane containerMessageFP;

	// Constructeur d'OnlineBP, garnissant la variable GameViewSP
	public OnlineBP(GameViewSP gvSP){
		this.gvSP = gvSP;
		players = new ArrayList<>();
		this.userName = gvSP.getUsrName();
		// Ajoute  le nom du joueur a la liste 
		players.add(userName);
		this.getChildren().addAll(getVbConteneur());
		try {
			// Tente de se connecter au serveur, et de creer les buffer de lecture et d'ecriture
			socket = new Socket("109.89.156.4", 5000);
			bufferedWriter = new ObjectOutputStream(socket.getOutputStream());
			bufferedWriter.flush();
			bufferedReader = new ObjectInputStream(socket.getInputStream());
			// Envoie un message de connexion au serveur 
			sendMessage(new Message(Operation.CONNECT, userName));
			// Demarre le service d'ecoute de messages
			listenForMessage();
		} catch (IOException e1) {
			e1.printStackTrace();
			closeEverything(socket, bufferedReader, bufferedWriter);
		}
		this.setId("onlineBP");
		
	}
	
	// Cette methode gere le bouton permettant d'indiquer que l'on est pret a jouer au serveur 
	public Button getBtnReady() {
		if(btnReady == null) {
			btnReady = new Button("Not ready");
			btnReady.getStyleClass().add("btnRed");
			btnReady.setOnAction(ev -> {
				// Inverse la couleur et le texte des boutons ready pour indiquer si le joueur est pret ou non, 
				// et envoie un message au serveur
				if(btnReady.getText().equals("Not ready") && players.size() >= 2) {
					btnReady.getStyleClass().remove(btnReady.getStyleClass().size()-1);
					btnReady.setText("Ready");
					btnReady.getStyleClass().add("btnGreen");
					sendMessage(new Message(Operation.READY, userName));
				}
				else {
					btnReady.getStyleClass().remove(btnReady.getStyleClass().size()-1);
					btnReady.setText("Not ready");
					btnReady.getStyleClass().add("btnRed");
				}
			});
			
		}
		return btnReady;
	}

	// Le bouton return envoie un message au serveur notifiant la fin de connexion 
	// et ferme le socket et les buffer
	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Return");
			btnReturn.setOnAction(ev -> 
			{
				// Envoie le message de deconnexion
				sendMessage(new Message(Operation.DISCONNECT, userName));
				// Affiche le menu principal et retire OnBP
				gvSP.rendreInvisibleSauf(gvSP.getMmBP());
				gvSP.getChildren().remove(gvSP.getOnBP());
				// Ferme le socket et les buffer
				closeEverything(socket, bufferedReader, bufferedWriter);
				gvSP.resetAllNode();
			});
			btnReturn.getStyleClass().add("btnLocalGame");
		}
		return btnReturn;
	}

	public VBox getVbConteneur() {
		if(vbConteneur == null) {
			vbConteneur = new VBox(new Label("Lobby"),getTxtAreaLobby(), new Label("Messages"), getTxtMessage(), getMessagesSP(),new HBox(getBtnReady(), getBtnReturn()));
			vbConteneur.setMinWidth(200);
			vbConteneur.setMaxWidth(500);
			vbConteneur.setSpacing(20);
			vbConteneur.setAlignment(Pos.CENTER);
		}
		return vbConteneur;
	}

	// Le champ de message permet d'envoyer un message aux autres joueurs
	public TextField getTxtMessage() {
		if(txtMessage == null) {
			txtMessage = new TextField();
			txtMessage.setOnKeyPressed(ev -> {
				if(ev.getCode() == KeyCode.ENTER) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							Label lb = new Label(txtMessage.getText());
							lb.setPrefWidth(vbConteneur.getMaxWidth() - 15.);
							lb.getStyleClass().add("lblBlue");
							lb.setAlignment(Pos.CENTER_RIGHT);
							getContainerMessageFP().getChildren().add(lb);
							sendMessage(new Message(Operation.MESSAGE, txtMessage.getText()));
							txtMessage.clear();
						}});
				}
			});
		}
		return txtMessage;
	}

	public TextArea getTxtAreaLobby() {
		if(txtAreaLobby == null) {
			txtAreaLobby = new TextArea();
			txtAreaLobby.setEditable(false);
		}
		return txtAreaLobby;
	}
	
	public ScrollPane getMessagesSP() {
		if(messagesSP == null) {
			messagesSP = new ScrollPane();
			messagesSP.setContent(getContainerMessageFP());
			messagesSP.setPrefHeight(450);
		}
		return messagesSP;
	}

	public FlowPane getContainerMessageFP() {
		if(containerMessageFP == null) {
			containerMessageFP = new FlowPane();
			containerMessageFP.setId("containerMessageOnline");
		}
		return containerMessageFP;
	}

	public String getUserName() {
		return userName;
	}
	
	// Permet d'envoyer un message au serveur
	public void sendMessage(Message message) {
		try {
			bufferedWriter.writeObject(message);
			bufferedWriter.flush();
		} catch(IOException e) {
			e.printStackTrace();
			closeEverything(socket, bufferedReader, bufferedWriter);
		}
	}
	
	// Cette methode permet d'ecouter les messages en provenance du serveur
	public void listenForMessage() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message message;
				if(socket != null) {
					while(socket.isConnected() && !socket.isClosed()) {
						try {
							message = (Message)bufferedReader.readObject();
							// Applique l'operation correspondante au message recu
							switch(message.getOperation()) {
								case MESSAGE: operationMessage(message.getMissive());
									break;
								case CONNECT: operationConnect(message.getMissive());
									break;
								case DISCONNECT: operationDisconnect(message.getMissive());
									break;
								case STARTGAME: operationStartGame();
									break;
								case ROLL:operationsRoll((int)message.getMissive());
									break; 
								case ON_TRUE_ANSWER: operationOnTrueAnswer((String)message.getMissive());
									break;
								case ON_FALSE_ANSWER:  operationOnFalseAnswer((String)message.getMissive());
									break;
								case QUESTION_CATEGORY:  operationQuestionCategory((Question)message.getMissive());
									break;
								case BONUS_CATEGORY: operationBonusCategory((Category)message.getMissive());
									break;
								case MALUS_CATEGORY: operationMalusCategory((Category)message.getMissive());
								break;
								default:
									break;
							}
							
						} catch(IOException | ClassNotFoundException e) {
							e.printStackTrace();
							closeEverything(socket, bufferedReader, bufferedWriter);
						}
					}
				}
			}
		}).start();
	}
	
	// Permet de fermer les sockets et les buffers
	public void closeEverything(Socket socket, ObjectInputStream bufferedReader2, ObjectOutputStream bufferedWriter2) {
		
		try {
			if(bufferedReader2 != null) {
				bufferedReader2.close();
			}
			if(bufferedWriter2 != null) {
				bufferedWriter2.close();
			}
			if(socket != null) {
				socket.close();
				this.socket = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Methode appellee pour recevoir un message dans le tchat
	public void operationMessage(Object missive) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Label lb = new Label(missive.toString());
				lb.setPrefWidth(vbConteneur.getMaxWidth() - 15.);
				getContainerMessageFP().getChildren().add(lb);
			}});
	}
	
	// Methode appellee lorsqu'un joueur se connecte
	@SuppressWarnings("unchecked")
	public void operationConnect(Object missive) {
		String tmp="";
		players.clear();
		players.addAll((List<String>) missive);
		
		for (String name : (List<String>)missive) {
			tmp += name + "\n";
		}
		getTxtAreaLobby().setText(tmp);
	}
	
	// Methode appellee quand un joueur se deconnecte
	@SuppressWarnings("unchecked")
	public void operationDisconnect(Object missive)
	{
		if(missive instanceof List<?>)
		{
			String tmp="";
			players.clear();
			players.addAll((List<String>) missive);
			
			for (String name : (List<String>)missive) {
				tmp += name + "\n";
			}
			getTxtAreaLobby().setText(tmp);
		}
	}
	
	// Methode appellee pour demarrer la partie
	public void operationStartGame() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				GenerationPionAvatar.fillList();
				gvSP.getChildren().add(gvSP.getOnDiSP());
				gvSP.rendreInvisibleSauf(gvSP.getOnDiSP());
				gvSP.getOnDiSP().initializePlayer(players);
				//gvSP.getOnDiSP().nextTurn();
				gvSP.getOnDiSP().displayPlayersInfo();
			}
		});
	}
	
	// Methode appellee lorsqu'un joueur a lance le de
	public void operationsRoll(int diceNumber)
	{
		gvSP.getOnDiSP().rollDice(diceNumber);
	}
	
	// Methode appellee quand un joueur repond correctement 
	public void operationOnTrueAnswer(String missive)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				for (Node n : gvSP.getOnDiSP().getvbAnswers().getChildren()) {
					Button b = (Button) n;
					if(b.getText().equals(missive))
					{
						b.setStyle("-fx-background-color: lightgreen");
					}
					
				}
				gvSP.getOnDiSP().getAnimationDecompte().stop();
				gvSP.getOnDiSP().validatePlayerAnswer();
				// Objet Timeline permettant de faire une temporisation avant la prochaine question. 
				Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1), ev->{
					gvSP.getOnDiSP().nextTurn();
				}));
				// Temporisation d'1 seconde
				tl.setCycleCount(1);
				tl.play();
			}
		});
		
	}
	
	// Methode appellee quand un joueur ne repond pas correctement 
	public void operationOnFalseAnswer(String missive)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				for (Node n : gvSP.getOnDiSP().getvbAnswers().getChildren()) {
					Button b = (Button) n;
					if(b.getText().equals(missive))
					{
						b.setStyle("-fx-background-color: lightcoral");
					}
					
				}
				gvSP.getOnDiSP().getAnimationDecompte().stop();
				// Objet Timeline permettant de faire une temporisation avant la prochaine question. 
				Timeline tl = new Timeline(new KeyFrame(Duration.seconds(1), ev->{
					gvSP.getOnDiSP().nextTurn();
				}));
				// Temporisation d'1 seconde
				tl.setCycleCount(1);
				tl.play();
			}
		});
	}
	
	// Methode appellee lorsqu'on recoit une question du serveur
	public void operationQuestionCategory(Question question)
	{
		Platform.runLater(new Runnable() {
			public void run() {
				gvSP.getOnDiSP().changeQuestion(question);
			}
		});
	}
	
	// Methode appellee pour la case defi donnant une categorie bonus
	public void operationBonusCategory(Category c)
	{
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				gvSP.getOnDiSP().givePlayerACategorie(c);
			}
		});
	}

	// Methode appellee pour la case defi retirant une categorie a tout le monde
	public void operationMalusCategory(Category c)
	{
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				gvSP.getOnDiSP().removeEveryoneACAtegory(c);
			}
		});
	}
}

package view.menus;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import resizestrategy.ResizeStrategy;
import resizestrategy.StrategyList;
import view.ApplyDropShadow;
import view.GameViewSP;

public class ParametersBP extends BorderPane {
	
	private GameViewSP gvSP;
	private VBox vbConteneur;

	private VBox vbGraphics;
	private Label lblGraphics;
	private HBox hbResolution;
	private Label lblResolution;
	private ComboBox<String> cbbResolution;
	private HBox hbFullScreen;
	private Label lblFullScreen;
	private ToggleButton chBFullScreen;
	
	private VBox vbAudio;
	private Label lblAudio;
	private HBox hbMusic;
	private Label lblMusic;
	private Slider sldMusic;
	private HBox hbAudioEffects;
	private Label lblAudioEffects;
	private Slider sldAudioEffects;
	
	private Button btnReturn;
	
	public ParametersBP(GameViewSP gvSP) {
		
		this.gvSP = gvSP;
		this.setCenter(getVbConteneur());
		BorderPane.setAlignment(getVbConteneur(), Pos.CENTER);
		ApplyDropShadow.applyDropShadow(this.getChildren());
		this.setId("parametersBP");
	}
	
	private VBox getVbConteneur() {
		if(vbConteneur == null) {
			vbConteneur = new VBox();
			vbConteneur.getChildren().addAll(getVbGraphics(), getVbAudio(), getBtnReturn());
			vbConteneur.setAlignment(Pos.CENTER);
		}
		return vbConteneur;
	}
	
	public VBox getVbGraphics() {
		if(vbGraphics == null) {
			vbGraphics = new VBox(getLblGraphics() ,getHbResolution(), getHbFullScreen());
			vbGraphics.setAlignment(Pos.CENTER);
		}
		return vbGraphics;
	}
	
	private Label getLblGraphics() {
		if(lblGraphics == null) {
			lblGraphics = new Label("Graphics");
			lblGraphics.getStyleClass().add("titleparam");
		}
		return lblGraphics;
	}
	private HBox getHbResolution() {
		if(hbResolution == null) {
			hbResolution = new HBox();
			hbResolution.getChildren().addAll(getLblResolution(), getCbbResolution());
			hbResolution.setAlignment(Pos.CENTER);
		}
		return hbResolution;
	}

	
	private Label getLblResolution() {
		if(lblResolution == null) {
			lblResolution = new Label("Resolution : ");
		}
		return lblResolution;
	}
	
	private ComboBox<String> getCbbResolution() {
		if(cbbResolution == null) {
			cbbResolution = new ComboBox<>();
			for(int i = 0 ; i < StrategyList.values().length ;i++) {
				cbbResolution.getItems().add(StrategyList.values()[i].getS());
			}
			cbbResolution.setOnAction(ev -> {
				try {
					Class<?> c = Class.forName("resizestrategy."+StrategyList.fromString(cbbResolution.getValue()).name());
					Constructor<?> cons = c.getConstructor();
					Object object = cons.newInstance();
					gvSP.setResizeStrategy((ResizeStrategy)object);
				} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			});
			cbbResolution.setValue(StrategyList.fromResizeStrategy(gvSP.getResizeStrategy()).getS());
			
		}
		return cbbResolution;
	}
	
	public HBox getHbFullScreen() {
		if(hbFullScreen == null) {
			hbFullScreen = new HBox();
			hbFullScreen.getChildren().addAll(getLblFullScreen(), getTgFullScreen());
			hbFullScreen.setAlignment(Pos.CENTER);
		}
		return hbFullScreen;
	}

	public Label getLblFullScreen() {
		if(lblFullScreen == null) {
			lblFullScreen = new Label("Fullscreen : ");
		}
		return lblFullScreen;
	}

	public ToggleButton getTgFullScreen() {
		if(chBFullScreen == null) {
			chBFullScreen = new ToggleButton();
			chBFullScreen.setOnAction(ev -> {
				if(chBFullScreen.isSelected()) {
					gvSP.getPrimaryStage().setFullScreen(true);
					gvSP.setFullScreen(true);
					
				} else {
					gvSP.getPrimaryStage().setFullScreen(false);
					gvSP.setFullScreen(false);
				}
			});
			if(gvSP.isFullScreen())
				chBFullScreen.setSelected(true);
			else
				chBFullScreen.setSelected(false);
		}
		return chBFullScreen;
	}

	public VBox getVbAudio() {
		if(vbAudio == null) {
			vbAudio = new VBox(getLblAudio(), getHbMusic(), getHbAudioEffects());
			vbAudio.setAlignment(Pos.CENTER);
		}
		return vbAudio;
	}

	public Label getLblAudio() {
		if(lblAudio == null) {
			lblAudio = new Label("Audio");
			lblAudio.getStyleClass().add("titleparam");
		}
		return lblAudio;
	}

	public HBox getHbMusic() {
		if(hbMusic == null) {
			hbMusic = new HBox(getLblMusic(), getSldMusic());
			hbMusic.setAlignment(Pos.CENTER);
		}
		return hbMusic;
	}

	public Label getLblMusic() {
		if(lblMusic == null) {
			lblMusic = new Label("Musique : ");
		}
		return lblMusic;
	}

	public Slider getSldMusic() {
		if(sldMusic == null) {
			sldMusic = new Slider(0,1,gvSP.getMusicManager().getMusicValue());
			sldMusic.setOnMouseDragged(ev -> {
				gvSP.getMusicManager().setVolume(sldMusic.getValue());
			});
		}
		return sldMusic;
	}
	
	public HBox getHbAudioEffects() {
		if(hbAudioEffects == null) {
			hbAudioEffects = new HBox(getLblAudioEffects(), getSldAudioEffects());
			hbAudioEffects.setAlignment(Pos.CENTER);
		}
		return hbAudioEffects;
	}

	public Label getLblAudioEffects() {
		if(lblAudioEffects == null) {
			lblAudioEffects = new Label("Audio effects : ");
		}
		return lblAudioEffects;
	}

	public Slider getSldAudioEffects() {
		if(sldAudioEffects == null) {
			sldAudioEffects = new Slider(0, 1, 1);
			
		}
		return sldAudioEffects;
	}

	public Button getBtnReturn() {
		if(btnReturn == null) {
			btnReturn = new Button("Return");
			btnReturn.setOnAction(ev -> {
				gvSP.rendreInvisibleSauf(gvSP.getMmBP());
				gvSP.getChildren().remove(gvSP.getPaBP());
				gvSP.resetAllNode();
			});
		}
		return btnReturn;
	}
}
